/* Copyright (C) 
* 2015 - zyuming zyumingfit@163.com
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.  * 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
* 
*/
/**
* @file skyeye_time.c
* @brief the skyeye time implementation
* @author zyuming zyumingfit@163.com
* @version 
* @date 2015-09-02
*/

#include "skyeye_mm.h"
#include "skyeye_sched.h"
#include "skyeye_queue.h"
#include "skyeye_attribute.h"
#include "skyeye_obj.h"
#include "skyeye_log.h"
#include <stdio.h>
#include "skyeye_system.h"
#include "skyeye_iface.h"
#include "skyeye_types.h"

skyeye_system_t *vir_system = NULL;

void system_init(){
	int i;
	vir_system = skyeye_mm_zero(sizeof(skyeye_system_t));
	vir_system->timer_cnt = 0;
	vir_system->soc_cnt = 0;
	vir_system->linker_cnt = 0;
	vir_system->dispatch_ready = False;
	for(i = 0; i < MAX_SOC; i++){
		sys_soc_t *soc = &(vir_system->socs[i]);
		soc->dev_cnt = 0;
		soc->cpu_cnt = 0;
		soc->timer_cnt = 0;
		soc->io_pass_count = 0;
		LIST_INIT(&(soc->handler_list));

	}
}

void system_clear(){
	int i, j;
	struct event *tmp ;
	struct event *q = NULL;

	for(i = 0; i < vir_system->soc_cnt;i++){
		sys_soc_t *sys_soc = &(vir_system->socs[vir_system->soc_cnt]);
		LIST_FOREACH(tmp, &(sys_soc->handler_list), list_entry){
			q = tmp;
			LIST_REMOVE(tmp, list_entry);		
			skyeye_free(q);
		}
		LIST_EMPTY(&sys_soc->handler_list);
	}
	skyeye_free(vir_system);
	vir_system = NULL;
}


void *system_get_struct(conf_object_t *obj)
{
	return obj->sys_struct;
}

sys_soc_t *system_register_soc(conf_object_t *soc){
	sys_soc_t *sys_soc = &(vir_system->socs[vir_system->soc_cnt]);
	vir_system->soc_cnt++;
	sys_soc->soc = soc;
	soc->sys_struct = (void *)sys_soc;
	return sys_soc;
}

sys_linker_t * system_register_linker(conf_object_t *linker){
	sys_linker_t *sys_linker = &(vir_system->linkers[vir_system->linker_cnt]);
	vir_system->linker_cnt++;
	linker->sys_struct = sys_linker;
	return sys_linker;
}

sys_dev_t * system_register_dev(conf_object_t *dev, conf_object_t *soc){
	sys_soc_t *sys_soc = (sys_soc_t *)system_get_struct(soc);

	sys_dev_t *sys_dev = &(sys_soc->devs[sys_soc->dev_cnt]);
	sys_soc->dev_cnt++;
	sys_dev->sys_soc = sys_soc;
	sys_dev->dev = dev;

	dev->sys_struct = sys_dev;	
	return  No_exp;
}

sys_cpu_t * system_register_cpu(conf_object_t *cpu, conf_object_t *soc){

	sys_soc_t *sys_soc = (sys_soc_t *)system_get_struct(soc);
	sys_cpu_t *sys_cpu = &(sys_soc->cpus[sys_soc->cpu_cnt]);
	sys_soc->cpu_cnt++;
	sys_cpu->sys_soc = sys_soc;
	sys_cpu->cpu = cpu;
	cpu->sys_struct = sys_cpu;
	return sys_cpu;
}

skyeye_system_t *system_get(){
	return vir_system;
}

static uint64_t gcd(uint64_t a,uint64_t b)
{
    if (0 == b) 
	    return a;
    return gcd(b, a % b);
}

static uint64_t gcdn(uint64_t *digits,uint64_t length)
{
    if (1 == length) 
	    return digits[0];
    else 
	    return gcd(digits[length - 1], gcdn(digits, length - 1));
}


static void seek_max_time_slice(sys_soc_t *soc)
{
	uint32_t freq_hz, i;	
	uint64_t us[MAX_TIMER];
	conf_object_t *cpu_obj;
	for(i = 0; i < soc->timer_cnt; i++){
		us[i] = (soc->timers[i])->us;
	}
	if(soc->timer_cnt != 0){
		soc->io_cycle_time = gcdn(us,soc->timer_cnt);
	}else
		soc->io_cycle_time = 1000;
	
	if (soc->cpu_cnt == 0){
		return;
	}else if(soc->cpu_cnt ==1){
		cpu_obj = soc->cpus[0].cpu;
		if(!SKY_has_attribute(cpu_obj, "freq_hz")){
			attr_value_t attr = SKY_get_attribute(cpu_obj, "freq_hz");
			soc->cpus[0].freq_hz = SKY_attr_uinteger(attr);
			freq_hz = soc->cpus[0].freq_hz;
		}else
			freq_hz = 25000000;
		soc->io_cycle_count = ((freq_hz * 0.8) / 1000000) * soc->io_cycle_time;
	}else{
		//fix me
		return ;
	}
	return ;
}

static void add_to_func_list(sys_soc_t *soc, sys_timer_t *timer)
{
	struct event* e;
	e = skyeye_mm(sizeof(struct event));
	e->timer = timer;
	e->target = timer->target;
	e->pass_count = 0;
	LIST_INSERT_HEAD(&(soc->handler_list), e, list_entry);
}

static void create_func_list(sys_soc_t *soc){
	struct event* e;
	int i;
	for(i = 0; i < soc->timer_cnt; i++){
		e = skyeye_mm(sizeof(struct event));
		e->timer = soc->timers[i];
		e->target = soc->timers[i]->target;
		e->pass_count = 0;
		LIST_INSERT_HEAD(&(soc->handler_list), e, list_entry);
	}
}

int fflush_events(sys_soc_t *sys_soc){
	struct event *tmp ;
	LIST_FOREACH(tmp, &(sys_soc->handler_list), list_entry){
		tmp->cycle_count = sys_soc->io_cycle_count * (tmp->timer->us / sys_soc->io_cycle_time);
	}
	return 0;
}

bool_t system_del_timer_handler(time_handle_t handle)
{
	if(handle < 0 || handle >= vir_system->timer_cnt)
		return False;
	vir_system->timers[(uint32_t)handle].enable = False;
	return True;

}
bool_t system_disable_timer(time_handle_t handle)
{
	if(handle < 0 || handle >= vir_system->timer_cnt)
		return False;
	vir_system->timers[(uint32_t)handle].enable = False;
	return True;

}
bool_t system_enable_timer(time_handle_t handle)
{
	if(handle < 0 || handle >= vir_system->timer_cnt)
		return False;
	vir_system->timers[(uint32_t)handle].enable = True;
	return True;

}

time_handle_t system_register_timer_handler(conf_object_t* obj, uint64_t us, sched_func_t func, uint32_t sched_count)
{
	sys_timer_t *timer = &(vir_system->timers[vir_system->timer_cnt++]);

	timer->target = obj;
	timer->us = us;
	timer->func = func;
	timer->enable = True;
	timer->sched_count = sched_count;
	timer->be_sched_count = 0;
	if(vir_system->dispatch_ready){
		sys_dev_t *sys_dev = obj->sys_struct;
		
		sys_dev->sys_soc->timers[sys_dev->sys_soc->timer_cnt++] = timer;
		seek_max_time_slice(sys_dev->sys_soc);
		add_to_func_list(sys_dev->sys_soc, timer);
		fflush_events(sys_dev->sys_soc);
	}

	return vir_system->timer_cnt - 1;
}



static void dispatch_timers_to_soc(void){
	int i;
	sys_timer_t *timers = vir_system->timers;
	for(i = 0; i < vir_system->timer_cnt; i++){
		sys_dev_t *dev = system_get_struct(timers[i].target);
		if(!dev){
			skyeye_log(Info_log, __FUNCTION__, "%s not a device object\n", (timers[i].target)->objname);
			continue;
		}
	
		dev->sys_soc->timers[dev->sys_soc->timer_cnt++] = timers + i;
	}
	vir_system->dispatch_ready = True;
}



exception_t system_clock_calculation(void){
	int i;
	dispatch_timers_to_soc();
	for(i = 0; i < vir_system->soc_cnt; i++){
		seek_max_time_slice(&(vir_system->socs[i]));
		create_func_list(&(vir_system->socs[i]));
		fflush_events(&(vir_system->socs[i]));
	}

	return No_exp;
}

void system_cpu_cycle(sys_cpu_t *sys_cpu){

	sys_soc_t * soc = sys_cpu->sys_soc;
	struct event *tmp;
	int steps;
	steps = sys_cpu->core_exec->run(sys_cpu->cpu);
	soc->io_pass_count +=steps;
	if(soc->io_pass_count >= soc->io_cycle_count){
		LIST_FOREACH(tmp, &(soc->handler_list), list_entry){
			tmp->pass_count +=soc->io_cycle_count;
			if(tmp->pass_count >= tmp->cycle_count && tmp->timer->enable == True){
				if(tmp->timer->sched_count == SCHED_NORMAL || tmp->timer->be_sched_count < tmp->timer->sched_count)
				{
					tmp->timer->func(tmp->target);
					tmp->pass_count = 0;
					tmp->timer->be_sched_count++;
				}
			}
		}		
		soc->io_pass_count = 0;
		soc->virtual_ms += soc->io_cycle_time;
	}
}
