/* Copyright (C) 
* 2011 - Michael.Kang blackfin.kang@gmail.com
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
* 
*/
/**
* @file sim_ctrl.c
* @brief the control function set of the simulator
* @author Michael.Kang blackfin.kang@gmail.com
* @version 
* @date 2011-04-30
*/

#include <stdlib.h>
#include <sim_control.h>
#include "skyeye_types.h"
#include "skyeye_config.h"
#include "skyeye_pref.h"
#include "skyeye_module.h"
#include "skyeye_arch.h"
#include "skyeye_callback.h"
#include "skyeye_cell.h"
#include "skyeye_internal.h"
#include "skyeye_checkpoint.h"
#include "skyeye_mm.h"
#include "skyeye_command.h"
#include "skyeye_loader.h"
#include "skyeye_symbol.h"
#include "skyeye_log.h"
#include "skyeye_cli.h"
#include "skyeye_class.h"
#include "portable/portable.h"
#include "json_conf.h"
#include "skyeye_mach.h"
#include "skyeye_internal.h"
#include "skyeye_system.h"

/* FIXME, we should get it from prefix varaible after ./configure */
#ifndef SKYEYE_MODULE_DIR
const char* default_lib_dir = "/opt/skyeye/lib/skyeye/";
#else
const char* default_lib_dir = SKYEYE_MODULE_DIR;
#endif
/**
* @brief the default cell of the simulator
*/
static skyeye_cell_t* default_cell = NULL;

/**
* @brief the flag of running or stop
*/
static bool_t SIM_running = False;
static bool_t SIM_started = False;
void SIM_init_command_line(void){
}

/**
* @brief initialization of environment
*
* @param argv
* @param handle_signals
*/
void SIM_init_environment(char** argv, bool_t handle_signals){
}

void SIM_main_loop(void){
}

/* we try to do init in batch mode */
static exception_t try_init(){
	return No_exp;
}

//void SIM_cli();
cli_func_t global_cli = NULL;
cli_func_t global_gui = NULL;

/**
* @brief all the initilization of the simulator
*/
void SIM_init(){
	sky_pref_t* pref;
	char* welcome_str = get_front_message();
	atexit(SIM_fini);
	/* 
	 * get the corrent_config_file and do some initialization 
	 */

	reset_skyeye_config();
	skyeye_config_t* config = get_current_config();
	skyeye_option_init(config);
	/*
	 * initialization of callback data structure, it needs to 
	 * be initialized at very beginning.
	 */
	init_callback();
	SKY_hap_init();

	/*
	 * initialization of system ctructure
	 * be initialized at very beginning.
	 */
	system_init();

	/*
	 * initilize the data structure for command
	 * register some default built-in command
	 */
	init_command_list();


	/*
	 * initialization of module manangement 
	 */
	init_module_list();
	
	/*
	 * initialization the timer scheduler 
	 */
	//init_thread_scheduler();
	//init_timer_scheduler();

	init_threads();
	
	/*
	 * initialization of architecture and cores
	 */
	init_arch();
	
	/*
	 * initialization of bus and memory module
	 */
	init_bus();


	/*
	 * initialization of machine module
	 */
	init_mach();

	/*
	 * initialization of common class module
	 */
	init_common_class();


	
	/*
	 * initialization of breakpoint, that depends on callback module.
	 */
	init_bp();

	/* the management of named object */
	//init_conf_obj();

	/*
	 * initialization of breakpoint, that depends on callback module.
	 */
	init_chp();

	/* 
	 * get the current preference for simulator 
	 */
	pref = get_skyeye_pref();

	/* 
	 * loading all the modules in search directory 
	 */
	if(!pref->module_search_dir)
#ifndef __WIN32__
	{
		pref->module_search_dir = skyeye_strdup(default_lib_dir);
	}
#else
	{
		char lib_dir[1024] = {'\0'};
		char *skyeyebin = getenv("SKYEYEBIN");
		
		if(strlen(skyeyebin) + strlen("/../lib/skyeye") + 1 > 1024){
			printf("The lib dir name is too long\n");
			skyeye_log(Error_log, __FUNCTION__, "The lib dir name is too log\n");
			exit(0);
		}
		strcat(lib_dir, skyeyebin);
		strcat(lib_dir,"/../lib/skyeye"); 
		pref->module_search_dir = skyeye_strdup(lib_dir);
	}
#endif

	//SKY_load_all_modules(pref->module_search_dir, NULL);
	/* save the original termios */
	struct termios tmp;
	tcgetattr(0, &tmp);
	memcpy(&pref->saved_term, &tmp, sizeof(struct termios));

	/*
	 * if we run simulator in GUI or external IDE, we do not need to
	 * launch our CLI.
	 */
	if(pref->gui_mode == True)
	{
		if(global_gui != NULL){
			global_gui("SkyEye");
			exit(0);
		}else
			printf("No gui found\n");
	}
	if(pref->interactive_mode == True){
		if(global_cli != NULL)
			global_cli("SkyEye");
		else
			printf("No cli found\n");
	}
	else{
		if (pref->autoboot == True) {
			char cmdstr[256] = {'\0'};
			sprintf(cmdstr, "run-script %s", pref->script_file);
			//RunCommand(cmdstr);
			SIM_run();
			while(1);
		}
		if (pref->autotest == True) {
			//RunAutotest();
		}
	}
}

/**
* @brief launch the simlator
*/
exception_t SIM_start(void){
	if(SIM_started){
		printf("SkyEye has been started\n");
		return Unknown_exp;
	}
	SIM_started = True;
	return No_exp;
}

/**
* @brief reset the simlator
*/
void  SIM_reset(void){
	/*set skyeye status to stop*/
	SIM_running = False;

	/*stop all cell */
	stop_all_cell();

	/*destroy all thread*/
	destroy_threads();

	/*destroy all objects*/
	SKY_delete_all_obj();

	/*clear system struct*/
	system_clear();

	/*init new thread fuction*/
	init_threads();

	/*init new thread scheduler thread*/
	init_thread_scheduler();

	/*init system struct*/
	system_init();
}

/**
* @brief quit the simlator
*/
void SIM_quit(void){
	/*set skyeye status to stop*/
	SIM_running = False;

	/*stop all cell */
	stop_all_cell();

	/*destroy all thread*/
	destroy_threads();

	/*destroy all obects*/
	SKY_delete_all_obj();

	/*clear system struct*/
	system_clear();

	/*quit skyeye*/
	exit(0);
}

/**
* @brief all the cli
*/
void SIM_cli(){
	skyeye_cli();
}

/**
* @brief set the running state for the simulator
*/
void SIM_run(){

	SIM_running = True;
	start_all_cell();
}

/**
* @brief continue the last stop state
*
* @param arch_instance
*/
void SIM_continue(generic_arch_t* arch_instance){
	//skyeye_continue();
	SIM_running = True;
	start_all_cell();
}

/**
* @brief stop the simulator
*
* @param arch_instance
*/
void SIM_stop(generic_arch_t* arch_instance){
	//skyeye_pause();
	SIM_running = False;
	stop_all_cell();
}

/**
* @brief if the simulator is in running state
*
* @param arch_instance
*/

bool_t SIM_is_running(){
	return SIM_running;
}

/**
* @brief destructor of the simulator
*/
extern void skyeye_erase_map(void);
void SIM_fini(){
	sky_pref_t *pref = get_skyeye_pref();
	/* unload all the module */
	//SKY_unload_all_modules();
	/* free the memory */
#ifndef __MINGW32__
	skyeye_erase_map();
#endif
#ifdef IS_MM_TEST
    skyeye_save_mm_info();
    skyeye_save_mm_result();
#endif
	printf("exit.\n");

#ifndef __MINGW32__
	/* restore the environment */
	tcsetattr(0, TCSANOW, &pref->saved_term);
#else
	tcsetattr(0, TCSAFLUSH, &pref->saved_term);
#endif

	return;
}
exception_t space_obj_free(addr_space_t* addr_space);
void reset_arch(void);
void del_from_cell(void);
/* 
 * Reatarting will close the skyeye process with the 100
 * The skyeye_controler detects number of skyeye exiting is 100
 * skyeye_controler will restart skyeye process
 */
void SIM_restart(void){
	if(!SIM_started){
		printf("SkyEye is very pure, you don't need to restart it!\n");
		return;
	}
#if 0
	skyeye_config_t* config = get_current_config();
	generic_arch_t* arch_instance = get_arch_instance("");
	if(config->mach == NULL){
		return;
	}
	if(config->mach->mach_fini)
		config->mach->mach_fini(arch_instance, config->mach);

	SIM_stop(arch_instance);
	space_obj_free(config->mach->phys_mem);
	/* Call SIM_exit callback */
	exec_callback(SIM_exit_callback, arch_instance);
#endif
	sky_pref_t *pref = get_skyeye_pref();
	printf("Destroy threads.\n");
	//destroy_threads();
	/* unload all the module */
	//SKY_unload_all_modules();

	reset_skyeye_config();
	/* reset mem map */
	reset_global_memmap();
	
	/* free exec from cell */
	del_from_cell();
	/* free the memory */
	skyeye_erase_map();
	/* reset arch */
	reset_arch();

	if(!pref->module_search_dir)
#ifndef __MINGW32__
		pref->module_search_dir = skyeye_strdup(default_lib_dir);
#else
	{
		char lib_dir[1024] = {'\0'};
		char *skyeyebin = getenv("SKYEYEBIN");
		
		if(strlen(skyeyebin) + strlen("/../lib/skyeye/") + 1 > 1024){
			printf("The lib dir name is too long\n");
			exit(0);
		}
		strcat(lib_dir, skyeyebin);
		strcat(lib_dir,"/../lib/skyeye/"); 
		pref->module_search_dir = skyeye_strdup(lib_dir);
	}
	
#endif
	//SKY_load_all_modules(pref->module_search_dir, NULL);
#ifndef __MINGW32__
	/* restore the environment */
	tcsetattr(0, TCSANOW, &pref->saved_term);
#else
	tcsetattr(0, TCSAFLUSH, &pref->saved_term);
#endif
	SIM_started = False;
}
#if 1
void register_cli(cli_func_t cli){
	global_cli = cli;
}
void register_gui(cli_func_t gui){
	global_gui= gui;
}
#endif
