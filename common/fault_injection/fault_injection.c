/**
 * @file addr_space.c
 * @brief The addr_space class
 * @author Michael.Kang blackfin.kang@gmail.com
 * @version 0.1
 * @date 2011-07-11
 */

#ifdef DEBUG
#undef DEBUG
#endif
//#define DEBUG
#include <skyeye_log.h>

#include <skyeye_types.h>
#include <memory_space.h>
#include <skyeye_addr_space.h>
#include <skyeye_mm.h>
#include "skyeye_obj.h"
#include <skyeye_dev_intf.h>
#include <skyeye_iface.h>
#include <skyeye_interface.h>
#include "skyeye_class.h"
#include  <stdlib.h>
#include <skyeye_fault_injection.h>

enum{
	and= 0,
	or = 1,
	xor = 2,
};

#if 0
void set_fault_injection(generic_address_t addr, int bitnum, int mode){

	switch (mode){
		case clr:
			fault_injection.ops = and;
			fault_injection.value = ~(1 << bitnum);
			break;
		case set:
			fault_injection.ops = or;
			fault_injection.value = 1 << bitnum;
			break;
		case negation:
			fault_injection.ops = xor;
			fault_injection.value = ~(1 << bitnum);
			break;
		default: 
			printf("no this fault injection mode, please set this mode correct!\n");	
	}
	fault_injection.addr = addr;
}
#endif

void read_plug(memory_space_plug_t* dev, generic_address_t addr, void *buf, size_t count){




		int tmp = *(int*)buf;
		int i;

		if(addr >= dev->fi_array.fi[0][0] && addr <= dev->fi_array.fi[dev->fi_array.count-1][0]){
			for(i = 0; i < dev->fi_array.count; i++){
				if(dev->fi_array.fi[i][0] == addr){
					do{
						switch(((fi_t*)(dev->fi_array.fi[i][1]))->mode){
							case and:
								*(int*)buf &= 1 << ((fi_t*)(dev->fi_array.fi[i][1]))->bitnum;
								break;
							case or:
								*(int*)buf |= 1 << ((fi_t*)(dev->fi_array.fi[i][1]))->bitnum;
								break;
							case xor:
								*(int*)buf ^= 1 << ((fi_t*)(dev->fi_array.fi[i][1]))->bitnum;
								break;
							default:
								skyeye_log(Info_log, __FUNCTION__, "no this falut injection command, please configure this option correct or close this option\n");
						}
					i++;
					}while((i < dev->fi_array.count) && (dev->fi_array.fi[i][0] == addr));
					break;
				}
			}
		}

		return ;
}

exception_t memory_space_plug_read(conf_object_t* obj, generic_address_t addr, void* buf, size_t count){

	memory_space_plug_t* dev_memory_space = obj->obj;
	dev_memory_space->memory_space->read(dev_memory_space->dev_memory_space_obj, addr, buf, count);
	read_plug(dev_memory_space, addr, buf, count);

      	return Not_found_exp;
}

exception_t memory_space_plug_write(conf_object_t* obj, generic_address_t addr, void* buf, size_t count){

	memory_space_plug_t* dev_memory_space = obj->obj;
	dev_memory_space->memory_space->write(dev_memory_space->dev_memory_space_obj, addr, buf, count);
	return Not_found_exp;
}


get_page_t *memory_space_plug_get_page(conf_object_t *obj, generic_address_t addr){

	memory_space_plug_t* dev_memory_space = obj->obj;
	dev_memory_space->memory_space->get_page(dev_memory_space->dev_memory_space_obj, addr);

	return NULL;
}

static conf_object_t* new_memory_space_plug(char* obj_name)
{
	memory_space_plug_t* dev= skyeye_mm_zero(sizeof(memory_space_plug_t));
	dev->obj = new_conf_object(obj_name, dev);

	return dev->obj;
}

static void free_memory_space_plug(conf_object_t *obj)
{
	return;
}

exception_t memory_space_save_plug_obj(conf_object_t* obj, conf_object_t* tmp_obj, generic_address_t addr, int bitnum, int mode){
	int i,j;
	fi_t *new_fi = malloc(sizeof(fi_t));
	memory_space_plug_t* dev_memory_space = obj->obj;

	//first time set dev_memory_space_obj
	if(tmp_obj){
		dev_memory_space->dev_memory_space_obj = tmp_obj;
		dev_memory_space->memory_space = SKY_get_iface(tmp_obj,MEMORY_SPACE_INTF_NAME);
	}

#if 0
	dev_memory_space->addr = addr;
	dev_memory_space->bitnum = bitnum;
	dev_memory_space->mode = mode;
#endif

	new_fi->addr = addr;
	new_fi->bitnum = bitnum;
	new_fi->mode = mode;
	dev_memory_space->fi_array.fi[dev_memory_space->fi_array.count][0] = addr;
	dev_memory_space->fi_array.fi[dev_memory_space->fi_array.count][1] = (unsigned int)new_fi;
	dev_memory_space->fi_array.count ++;
#if 0
	for(i = 0; i <= dev_memory_space->fi_array.count; i ++){
		if(dev_memory_space->fi_array.fi[i][0] && addr > dev_memory_space->fi_array.fi[i][0])
			continue;
		else{

			if(i == dev_memory_space->fi_array.count){
				dev_memory_space->fi_array.fi[i][0] = addr;
				dev_memory_space->fi_array.fi[i][1] = (unsigned int)new_fi;
			}else{
				for(j = dev_memory_space->fi_array.count-1; j >= i; j --){

					dev_memory_space->fi_array.fi[j+1][0] = dev_memory_space->fi_array.fi[j][0];
					dev_memory_space->fi_array.fi[j+1][1] = dev_memory_space->fi_array.fi[j][1];
				}
				dev_memory_space->fi_array.fi[i][0] = addr;
				dev_memory_space->fi_array.fi[i][1] = (unsigned int)new_fi;
			}
		}
	}
	/*
	for(i = 0; i <= dev_memory_space->fi_array.count; i ++){
		printf("%d:  %x    ",i,((fi_t*)(dev_memory_space->fi_array.fi[i][1]))->addr);
		printf("%x    ",((fi_t*)(dev_memory_space->fi_array.fi[i][1]))->bitnum);
		printf("%x\n",((fi_t*)(dev_memory_space->fi_array.fi[i][1]))->mode);
	}*/
	dev_memory_space->fi_array.count ++;
#endif

}

conf_object_t*  memory_space_clear_plug_obj(conf_object_t* obj, generic_address_t addr, int bitnum, int mode){
	memory_space_plug_t* dev_memory_space = obj->obj;
	int i,j;

	if(dev_memory_space->fi_array.count){
		for(i = 0; i < dev_memory_space->fi_array.count; i ++){
			if(((fi_t*)(dev_memory_space->fi_array.fi[i][1]))->addr != addr){
				continue;
			}else{
				if(((fi_t*)(dev_memory_space->fi_array.fi[i][1]))->bitnum == bitnum && ((fi_t*)(dev_memory_space->fi_array.fi[i][1]))->mode == mode){
					dev_memory_space->fi_array.fi[i][0] = 0;
					free(dev_memory_space->fi_array.fi[i][1]);
					for( j = i; j < dev_memory_space->fi_array.count; j ++){
						dev_memory_space->fi_array.fi[j][0] = dev_memory_space->fi_array.fi[j+1][0];
						dev_memory_space->fi_array.fi[j][1] = dev_memory_space->fi_array.fi[j+1][1];
					}
					dev_memory_space->fi_array.fi[dev_memory_space->fi_array.count][0] = 0;
					free(dev_memory_space->fi_array.fi[dev_memory_space->fi_array.count][1]);
					i--;
					dev_memory_space->fi_array.count --;
				}
			}
		}
		if(dev_memory_space->fi_array.count == 0){
			return dev_memory_space->dev_memory_space_obj;
		}else
			return NULL;
	}
}

void* memory_space_get_plug_obj(conf_object_t* obj){
	memory_space_plug_t* dev_memory_space = obj->obj;
	int i,j;

	if(dev_memory_space->fi_array.count){
		return &dev_memory_space->fi_array;
	}
	
	return NULL;
}

void memory_space_plug_register_interface(conf_class_t* clss) {
	static const memory_space_intf io_memory = {
		.read = memory_space_plug_read,
		.write = memory_space_plug_write,
		.get_page = memory_space_plug_get_page,
	};
	SKY_register_iface(clss, MEMORY_SPACE_PLUG_INTF_NAME, &io_memory);	
	
	static const memory_space_plug_save_intf save_plug = {
		.save_plug_obj = memory_space_save_plug_obj,
		.get_plug_obj = memory_space_get_plug_obj,
		.clear_plug_obj = memory_space_clear_plug_obj,
	};
	SKY_register_iface(clss, MEMORY_SPACE_PLUG_SAVE_INTF_NAME, &save_plug);	
}

void init_memory_space_plug(){
	static skyeye_class_t class_data = {
			.class_name = "memory_space_plug",
			.class_desc = "memory_space_plug",
			.new_instance = new_memory_space_plug,
			.free_instance = free_memory_space_plug,
			.get_attr = NULL,
			.set_attr = NULL
	};
	conf_class_t* clss = SKY_register_device_class(class_data.class_name, &class_data);
	memory_space_plug_register_interface(clss);
	return ;
}
