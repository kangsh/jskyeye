#ifndef __DEV_BUS_INTF_
#define __DEV_BUS_INTF_


/*i2c bus interface*/
typedef  int i2c_device_flag_t;
#define BUS_BUSY       -1
#define ADDR_ERROR     -2
#define SLAVE_ERROR    -3
#define NO_COM    -4

typedef struct skyeye_i2c_bus_interface{
	conf_object_t *conf_obj;
	int (*start)(conf_object_t* i2c_bus, uint8 address);
	int (*stop)(conf_object_t* i2c_bus);
	uint8 (*read_data)(conf_object_t* i2c_bus);
	void (*write_data)(conf_object_t* i2c_bus, uint8 value);
	int (*register_device)(conf_object_t* i2c_bus, conf_object_t* device, uint8 address, uint8 mask, i2c_device_flag_t flags);
	void (*unregister_device)(conf_object_t* i2c_bus, conf_object_t* device, uint8 address, uint8 mask);
}i2c_bus_intf;

#define I2C_BUS_INTF_NAME "i2c_bus"

typedef struct skyeye_std_1553b_bus_interface{
	conf_object_t *conf_obj;
	int (*start)(conf_object_t* std_1553b_bus, uint8 address);
	int (*stop)(conf_object_t* std_1553b_bus);
	int (*read_data)(conf_object_t* std_1553b_bus, conf_object_t* obj2, void* recv_buf, uint32 length, int sub_address);
	void (*write_data)(conf_object_t* std_1553b_bus, conf_object_t* obj2, void* send_buf, uint32 length, int RT_SubId);
	int (*register_device)(conf_object_t* std_1553b_bus, conf_object_t* device, uint32 dev_id);
	void (*unregister_device)(conf_object_t* std_1553b_bus, conf_object_t* device);
}std_1553b_bus_intf;

#define STD_1553B_BUS_INTF_NAME "std_1553b_bus_intf"

/*spi bus interface*/
typedef struct skyeye_spi_bus_interface{
	conf_object_t *conf_obj;
	int (*spi_slave_request)(conf_object_t *spi_bus, int first, int last, char *buf, int len, char *feedback);
	int (*connect_slave)(conf_object_t *spi_bus, char *slave_name);
	int (*disconnect_slave)(conf_object_t *spi_bus, char *slave_name);
	int  (*register_device)(conf_object_t* spi_bus, conf_object_t* device);
	void (*unregister_device)(conf_object_t* spi_bus, conf_object_t* device);

	void (*spi_bus_receive)(void *spi_bus, char *buf, int len);
	void (*reset_fifo)(void *spi_bus);
}spi_bus_intf;

#define SPI_BUS_INTF_NAME "spi_bus"


typedef struct{
	uint32_t ide;
	uint32_t id;
	uint32_t dlc;
	uint8_t data[8];
	uint32_t crc;
	uint32_t ack_slot;
}can_msg_t;

typedef struct{
	int (*send_msg)(conf_object_t *can_linker, conf_object_t *can_device, can_msg_t *msg);
	int (*send_data)(conf_object_t *can_linker, conf_object_t *can_device, void *buf, uint32_t len);
}can_linker_intf;
#define CAN_LINKER_INTF "can_linker_intf"


typedef struct{
	uint32_t ide;
	uint32_t id;
	uint32_t dlc;
	uint32_t data[4];
	uint32_t crc;
	uint32_t ack_slot;
}tsi578_msg_t;
typedef struct{
	int (*send_msg)(conf_object_t *tsi578_linker, conf_object_t *tsi578_device, tsi578_msg_t *msg);
}tsi578_linker_intf;
#define TSI578_LINKER_INTF "tsi578_linker_intf"

typedef struct{
	uint32_t desc[4];
}atm_msg_t;
typedef struct{
	int (*send_msg)(conf_object_t *atm_linker, conf_object_t *atm_device, atm_msg_t *msg);
	int (*send_data)(conf_object_t *atm_linker, conf_object_t *atm_device, void *buf, uint32_t len);
}atm_linker_intf;
#define ATM_LINKER_INTF "atm_linker_intf"

typedef struct Rs485_bus_interface{
	conf_object_t *conf_obj;
	int (*read_data)(conf_object_t* conf_obj, conf_object_t* device, void* recv_buf, uint32 dev_id, uint32 length);
	void (*write_data)(conf_object_t* conf_obj, conf_object_t* device, void *buf, uint32 dev_id, uint32 length);
	int (*register_device)(conf_object_t* conf_obj, conf_object_t* device, uint8 dev_id);
	void (*unregister_device)(conf_object_t* conf_obj, conf_object_t* device, uint8 dev_id);
}Rs485_bus_intf;

#define RS485_BUS_INTF "Rs485_bus_intf"

typedef struct Rs232_bus_interface{
	conf_object_t *conf_obj;
	int (*read_data)(conf_object_t* conf_obj, conf_object_t* device, void* recv_buf, uint32 dev_id, uint32 length);
	void (*write_data)(conf_object_t* conf_obj, conf_object_t* device, void *buf, uint32 dev_id, uint32 length);
	int (*register_device)(conf_object_t* conf_obj, conf_object_t* device, uint8 dev_id);
	void (*unregister_device)(conf_object_t* conf_obj, conf_object_t* device, uint8 dev_id);
}Rs232_bus_intf;

#define RS232_BUS_INTF "Rs232_bus_intf"

typedef struct Lvds_bus_interface{
	conf_object_t *conf_obj;
	int (*read_data)(conf_object_t* conf_obj, conf_object_t* device, void* recv_buf, uint32 dev_id, uint32 length);
	void (*write_data)(conf_object_t* conf_obj, conf_object_t* device, void *buf, uint32 dev_id, uint32 length);
	int (*register_device)(conf_object_t* conf_obj, conf_object_t* device, uint8 dev_id);
	void (*unregister_device)(conf_object_t* conf_obj, conf_object_t* device, uint8 dev_id);
}Lvds_bus_intf;

#define LVDS_BUS_INTF "Lvds_bus_intf"

typedef struct arinc_bus_interface{
	conf_object_t *conf_obj;
	int (*read_data)(conf_object_t* conf_obj, conf_object_t* device, void* recv_buf, uint32 dev_id, uint32 length);
	void (*write_data)(conf_object_t* conf_obj, conf_object_t* device, void *buf, uint32 dev_id, uint32 length);
	int (*register_device)(conf_object_t* conf_obj, conf_object_t* device, uint8 dev_id);
	void (*unregister_device)(conf_object_t* conf_obj, conf_object_t* device, uint8 dev_id);
}arinc_bus_intf;

#define ARINC_BUS_INTF "arinc_bus_intf"
typedef enum{
	BC_TO_RT,  //0
	RT_TO_RT,  //1
	RT_TO_BC,  //2
	RT_TO_RT_BROADCAST, //3
	BROADCAST, //4
	MODE_CODE_NO_DATA, //5
	TX_MODE_CODE_WITH_DATA, //6
	RX_MODE_CODE_WITH_DATA, //7
	BROADCAST_MODE_CODE_NO_DATA, //8
	BROADCAST_MODE_CODE_WITH_DATA,//9
	//MESG_TYPE_CNT is not mesg type
	MESG_TYPE_CNT,
}mesg_type_t;

typedef enum{
	BC_MODE,
	MT_MODE,
	RT_MODE, 
}std_1553b_mode_t;

typedef enum{
	COMMAND_WORD, //0
	DATA_WORD,    //1
	LAST_DATA_WORD,//2
	LAST_DATA_STATUS_WORD,//3
	DATA_WORD_LOOPED_BACK,//4
	LAST_DATA_WORD_LOOPED_BACK,//5
	TRANSMIT_COMMAND_WORD_LOOPED_BACK,//6
	BROADCAST_MODE_COMMAND_LOOPED_BACK,//7
	MODE_COMMAND_WORD_LOOPED_BACK,//8
	STATUS_WORD,//9
	TX_RT_STATUS_WORD,//10
	RX_RT_STATUS_WORD,//11
	//WORD_TYPE_CNT is not word type
	WORD_TYPE_CNT,
	NLL,
}word_type_t;


typedef struct{
	word_type_t type;
	mesg_type_t mesg_type;
	uint16_t value;
}std_1553b_word_t;

typedef enum{
	RT_TO_RT_TRANSMIT,
	RT_TO_RT_RECEIVE,
}std_1553b_mesg_status_t;

typedef struct{
	mesg_type_t type;
	uint32_t word_cnt;
	uint32_t rtr_status;//RT->RT, rt use this flag to identification data from BC or another RT
	uint32_t bc_mark; // bc used this value to mark the location of the data block
	uint32_t rt_mark; // rt used this value to mark the location of the data block
	std_1553b_word_t words[64]; //use up to 37 words, RT-to-RT transfer of 32 data words: 2 commands + loopback + \
	                            //2 status words + 32 data words = 37
}std_1553b_mesg_t;

typedef int position_t;
typedef struct{
	int (*bc_send_message)(conf_object_t *linker, std_1553b_mesg_t *message);  //LINKER implement the function
	int (*rt_receive_message)(conf_object_t *rt, std_1553b_mesg_t *message);   //RT implement the function
	int (*reponse_word)(conf_object_t *linker, conf_object_t *dev, std_1553b_word_t *word, uint32_t subaddress); //LINKER implement the function,it mark use subaddress 
	position_t (*bc_receive_word)(conf_object_t *bc, std_1553b_word_t *word, uint32_t bc_mark); //BC implement the function
	int (*register_address)(conf_object_t *linker, conf_object_t *dev, uint32_t address); //LINKER implement the function
	int (*set_mode)(conf_object_t *linker, conf_object_t *dev, std_1553b_mode_t mode); //LINKER implement the function
	int (*set_address)(conf_object_t *linker, conf_object_t *dev, uint16_t address);  //LINKER implement the function
}std_1553b_intf;
#define STD_1553B_INTF  "std_1553b_intf"


#endif
