#ifndef __SKYEYE_DEV_INTF_
#define __SKYEYE_DEV_INTF_

#include "skyeye_dev_bus_intf.h"
typedef int i2c_device_state_t;
#define I2C_IDLE           -1   
#define I2C_SLAVE_RECEIVE  0
#define I2C_SLAVE_TRANSMIT 1

typedef struct i2c_device_interface{
	conf_object_t *obj;
	int (*set_state)(conf_object_t* device, i2c_device_state_t state, uint8_t address);
	uint8_t (*read_data)(conf_object_t*device);
	void (*write_data)(conf_object_t*device, uint8_t value);
	uint8_t (*get_address)(conf_object_t*device);
}i2c_device_intf;

#define I2C_DEVICE_INTF_NAME "i2c_device"



/*SPI SLAVE DEVICE INTERFACE*/
typedef struct spi_device_interface{
	conf_object_t *obj;
	void (*spi_request)(conf_object_t *slave, int first, int last, char *buf, int len, char *feedback);
	int (*connect_master)(conf_object_t *slave);
	int (*disconnect_master)(conf_object_t *slave);

	void (*spi_device_receive)(conf_object_t *slave, char *buf, int len);
	void (*spi_device_receive_done)(conf_object_t *slave);
	void (*spi_deassert_cs_line)(conf_object_t *slave);
	void (*spi_assert_cs_line)(conf_object_t *slave);
	int (*spi_get_cs_line)(conf_object_t *slave);
}spi_device_intf;

#define SPI_DEVICE_INTF_NAME "spi_device"

/*GDB INTERFACE*/
#define  GDB_SERVER_ERROR -1
#define  GDB_SERVER_SUCCESS 0
typedef struct skyeye_gdbserver_interface{
	int *(*GdbServerStart)(conf_object_t *obj, char *cpuname, int port, char *ip);
	int *(*GdbServerIsConnected)(conf_object_t *obj);
	char *(*GdbServerGetClientIp)(conf_object_t *obj);
}skyeye_gdbserver_intf;
#define SKYEYE_GDBSERVER_INTF_NAME "skyeye_gdbserver"


/*GPIO connector interface*/
typedef struct skyeye_gpio_connector_interface{
	exception_t (*update)(conf_object_t *obj, uint32_t regvalue);
}skyeye_gpio_connector_intf;
#define SKYEYE_GPIO_CONNECTOR "gpio_connect"

typedef struct skyeye_gate_a_interface{
	int (*update)(conf_object_t *obj, uint32_t regvalue);
}skyeye_gate_a_intf;
#define LOGIC_GATE_A "gate_input_a"


typedef struct skyeye_gate_b_interface{
	int (*update)(conf_object_t *obj, uint32_t regvalue);
}skyeye_gate_b_intf;
#define LOGIC_GATE_B "gate_input_b"


typedef struct image_info{
	uint32_t (*get_page_size)(conf_object_t *obj);
	uint32_t (*get_page_count)(conf_object_t *obj);
	int32_t (*get_page_index_by_id)(conf_object_t *obj, uint32_t id);
	char *(*get_page_content)(conf_object_t *obj, uint32_t index);
	exception_t (*set_page_content)(conf_object_t *obj, void *buf, uint32_t index);
	exception_t (*clear_all_pages)(conf_object_t *obj);
	void (*set_image_size)(conf_object_t *obj, uint32_t size);
}skyeye_image_info;
#define SKYEYE_IMAGE_INFO "image_info"

/*BU61580 interface*/
typedef struct bu61580_dev_iface{
	conf_object_t* conf_obj;
	exception_t (*read) (conf_object_t *conf_obj, void *buf, size_t length);
	exception_t (*write) (conf_object_t *conf_obj, void *buf, size_t length);
	exception_t (*set_state) (conf_object_t *conf_obj, int length);
}bu61580_dev_intf;
#define BU61580_DEV_INTF_NAME "bu61580_dev_intf"


/*uart file interface*/
typedef struct uart_file_iface{
	exception_t (*read) (conf_object_t *conf_obj, void *buf, size_t length);
	exception_t (*write) (conf_object_t *conf_obj, void *buf, size_t length);
}uart_file_intf;
#define UART_FILE_INTF "uart_file_intf"

/*uart com interface*/
typedef struct uart_com_iface{
	exception_t (*read) (conf_object_t *conf_obj, void *buf, size_t length);
	exception_t (*write) (conf_object_t *conf_obj, void *buf, size_t length);
}uart_com_intf;
#define UART_COM_INTF "uart_com_intf"

typedef struct  instr_process_iface{
	int (*get_dif_pc_cache)(conf_object_t *obj, char *fname);
	int (*get_pc_record_size)(conf_object_t *obj);
	int (*set_pc_record_size)(conf_object_t *obj, int size);
	int (*get_pc_record_nums)(conf_object_t *obj);
	int (*get_pc_record_index)(conf_object_t *obj);
	int (*get_pc_record_overflow)(conf_object_t *obj);
	generic_address_t (*get_pc_by_index)(conf_object_t *obj, int n);
}instr_process_intf;
#define INSTR_PROCESS_INTF "instr_process_intf"

typedef struct{
	int (*receive_msg)(conf_object_t *can_device, can_msg_t *msg);
}can_dev_intf;
#define CAN_DEV_INTF "can_dev_intf"

typedef struct{
	int (*receive_msg)(conf_object_t *atm_device, atm_msg_t *msg);
}atm_dev_intf;
#define ATM_DEV_INTF "atm_dev_intf"

typedef struct{
	int (*receive_msg)(conf_object_t *tsi578_device, tsi578_msg_t *msg);
}tsi578_dev_intf;
#define TSI578_DEV_INTF "tsi578_dev_intf"

typedef struct{
	exception_t (*write)(conf_object_t *emif_device, generic_address_t addr, void *buf, size_t count);
	exception_t (*read)(conf_object_t *emif_device, generic_address_t addr, void *buf, size_t count);
}emif_memory_intf;
#define EMIF_MEMORY_INTF "emif_memory_intf"

typedef struct{
	exception_t (*read) (conf_object_t *conf_obj, generic_address_t offset, void *buf, size_t length);
	exception_t (*write)(conf_object_t *conf_obj, generic_address_t offset, void *buf, size_t length);
}bu61580_ram_intf;
#define BU61580_RAM_INTF "bu61580_ram_intf"

typedef enum{
	FIFOC_CMD = 0,
	FIFOC_INT = 0x4,
	FIFOC_DMA = 0x8,
	FIFOC_AXE = 0xc,
	FIFOC_DEBUG = 0x10
}mpc5121_sfifoc_reg_t;

typedef struct{
	void (*write_register)(conf_object_t *obj, mpc5121_sfifoc_reg_t reg, uint32_t value, reg_op_t op);
	void (*read_register)(conf_object_t *obj, mpc5121_sfifoc_reg_t reg, uint32_t *value, reg_op_t op);
}mpc5121_sfifoc_intf;
#define MPC5121_SFIFOC_INTF "mpc5121_sfifoc_intf"

typedef struct{
	exception_t (*transmit_complete)(conf_object_t *conf_obj, size_t length);
}edma_dev_intf;
#define EDMA_DEV_INTF "edma_dev_intf"

typedef struct{
	exception_t (*write_sound)(conf_object_t *conf_obj, void *buf, size_t length);
	exception_t (*read_sound)(conf_object_t *conf_obj, void *buf, size_t length);
}sound_intf;
#define SOUND_INTF "sound_intf"

#endif


