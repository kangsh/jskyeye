#ifndef __DEFAULT_COMMANDS_H__
#define __DEFAULT_COMMANDS_H__
int com_run(char* arg);
int com_cont(char* arg);
int com_stop(char* arg);
int com_si(char* arg);
int com_view(char* arg);
int com_rename(char* arg);
int com_delete(char* arg);
int com_list(char* arg);
int com_stat(char* arg);
int com_start(char* arg);
int com_cd(char* arg);
int com_pwd(char* arg);
int com_quit(char* arg);
int com_list_modules(char* arg);
int com_list_options(char* arg);
int com_list_machines(char* arg);
int com_show_pref(char* arg);
int com_load_module(char* arg);
int com_load_conf(char* arg);
int com_show_map(char* arg);
int com_info(char* arg);
int com_x(char* arg);
exception_t run_command(char* command_str);

uint32 gui_x(char *addr); char *gui_info_register(void); void info_register_free(char *register_p);
char *gui_get_current_mach(void);
char *gui_get_device_list(void);
uint64_t gui_get_step(void);
int gui_check_uart_pc(char *port);
int gui_get_cpu_rate(void);
int gui_get_serial_num(void);
char *gui_get_serial_objname_by_id(int id);

int32_t WIN_load_binary(char *cpuname, char *elfname);
int WIN_create_mach(char *objname, char *classname);
int WIN_create_cpu(char *machname ,char *objname, char *classname);
int WIN_create_device(char *machname ,char *objname, char *classname);
int WIN_load_file(char *cpuname, const char* filename, generic_address_t load_addr);
int WIN_connect(char *con_objname, char *iface_objname, char *name, uint32_t index);
int prepare_to_run(void);
int WIN_memory_space_add_map(char *memory_space_name, char *device_name, uint32_t address, uint32_t length);
int WIN_set_attr(char *objname, char *key, char *attr_type, char *value);
#endif
