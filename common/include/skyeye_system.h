#ifndef SKYEYE_SYSTEM
#define SKYEYE_SYSTEM

#ifdef __cplusplus
 extern "C" {
#endif


#include "skyeye_sched.h"
#include "skyeye_core_intf.h" 
#include "skyeye_queue.h"

#define  MAX_TIMER 64
#define  MAX_CPU 16
#define  MAX_DEV 64
#define  MAX_SOC 64
#define MAX_LINKER 64


#define SCHED_NORMAL -1
typedef int32_t time_handle_t;

typedef struct sys_soc sys_soc_t;

typedef struct{
	conf_object_t *target;
	uint64_t us;
	sched_func_t func;
	bool_t enable;
	uint32_t sched_count;
	uint32_t be_sched_count;
}sys_timer_t;

struct event{
	conf_object_t *target;
	uint64_t cycle_count;
	uint64_t pass_count;
	sys_timer_t *timer;
	LIST_ENTRY (event)list_entry;	
};


typedef struct{
	conf_object_t *cpu;
	char *sys_name;
	sys_soc_t *sys_soc;
	uint32_t freq_hz;  //HZ
	core_exec_intf* core_exec;
}sys_cpu_t;

typedef struct {
	conf_object_t *dev;
	sys_soc_t *sys_soc;
}sys_dev_t;

typedef struct {
	conf_object_t *linker;
}sys_linker_t;


struct sys_soc{
	conf_object_t *soc;
	sys_dev_t devs[MAX_DEV];
	sys_cpu_t cpus[MAX_CPU];
	sys_linker_t linkers[MAX_LINKER];
	sys_timer_t *timers[MAX_TIMER];

	/*The number of time handler included in the soc*/
	uint32_t timer_cnt;
	/*The number of devicevs included in the soc
	 */
	uint32_t dev_cnt;
	/*The number of cpus included in the soc
	  */
	uint32_t cpu_cnt;
	/*The number of cpus included in the soc
	  */
	uint32_t linker_cnt;

	/* After a certain number of instructions executed by 
	 * processor, need to dispatch IO device callback function
	 * */
	uint64_t io_cycle_count;
	uint64_t io_pass_count;

	/* After a certain number of instructions executed by 
	 * processor, need to dispatch IO device callback function
	 * */
	uint64_t io_cycle_time;


	/*system virtual time, millisecond
	 * */
	uint64_t virtual_ms;


	LIST_HEAD(timer_handler_head, event) handler_list;
};

typedef struct{
	sys_timer_t timers[MAX_TIMER];
	sys_soc_t socs[MAX_SOC];
	sys_linker_t linkers[MAX_LINKER];
	uint32_t timer_cnt;
	uint32_t soc_cnt;
	uint32_t linker_cnt;
	uint32_t dispatch_ready;
}skyeye_system_t;



exception_t system_clock_calculation(void);

skyeye_system_t *system_get();

void *system_get_struct(conf_object_t *obj);

 time_handle_t system_register_timer_handler(conf_object_t* obj, uint64_t us, sched_func_t func, uint32_t seched_count);
bool_t system_disable_timer(time_handle_t handle);
bool_t system_enable_timer(time_handle_t handle);

sys_cpu_t *system_register_cpu(conf_object_t *cpu, conf_object_t *soc);

sys_dev_t *system_register_dev(conf_object_t *dev, conf_object_t *soc);

sys_linker_t *system_register_linker(conf_object_t *linker);

sys_soc_t *system_register_soc(conf_object_t *soc);
void system_cpu_cycle(sys_cpu_t *cpu);

exception_t system_clock_calculation(void);

void system_clear();

void system_init();

#ifdef __cplusplus
}
#endif


#endif


