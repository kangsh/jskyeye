/* Copyright (C) 
* 2013 - Michael.Kang blackfin.kang@gmail.com
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
* 
*/
/**
* @file default_command.c
* @brief The implementation of default command
* @author Michael.Kang blackfin.kang@gmail.com
* @version 7849
* @date 2013-10-14
*/


#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <malloc.h>
#include <libgen.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>

#include "skyeye_class.h"
#include <skyeye_cli.h>
#include "skyeye_readline_cli.h"
#include "skyeye_module.h"
#include "skyeye_options.h"
#include "sim_control.h"
#include "skyeye_mach.h"
#include "skyeye_pref.h"
#include "skyeye_core.h"
#include "json_conf.h"
#include "bank_defs.h"
#include "skyeye_mm.h"
#include "default_command.h"
#include "skyeye_addr_space.h"
#include <skyeye_core_intf.h>
#include <skyeye_exec.h>
#include <json_conf.h>
#include <skyeye_cell.h>
#include <skyeye_iface.h>
#include <skyeye_attribute.h>
#include <skyeye_loader.h>
#include "breakpoint.h"
#include "skyeye_internal.h"
#include "skyeye_interface.h"
#include "skyeye_types.h"
#include "skyeye_checkpoint.h"
#include "skyeye_module.h"
#include "skyeye_int_register.h"
#include "skyeye_fault_injection.h"
#include "skyeye_x86devs_fin.h"
#include "skyeye_x86devs_intf.h"
#include "skyeye_callback.h"
#include "skyeye_system.h"
#include "systemc_exec.h"

#define Error 0
#define Ok 1

 //#include "arm_regformat.h"
 /*
  * Run SkyEye from the beginning 
@@ -1378,3 +1421,37 @@ void print_class_list()
 ret:
 	return ;
 }
*/
//#include "arm_regformat.h"

static char *tempPoint = NULL;
int com_run (arg)
     char *arg;
{
  if (!arg)
    arg = "";
  SIM_run();
  return 0;

  //sprintf (syscom, "ls -FClg %s", arg);
  //return (system (syscom));
}

/*
 * Continue running of the interrupted SkyEye 
 */
int com_cont (arg)
     char *arg;
{
  if (!arg)
    arg = "";
  SIM_continue(NULL);
  return 0;
  //sprintf (syscom, "ls -FClg %s", arg);
  //return (system (syscom));
}

/*
 * stop running of  SkyEye 
 */
int com_stop (arg)
     char *arg;
{
  if (!arg)
    arg = "";
  SIM_stop(NULL);
  return 0;
  //sprintf (syscom, "ls -FClg %s", arg);
  //return (system (syscom));
}

/*
 * stop running of  SkyEye 
 */

int com_start (arg)
     char *arg;
{
	int flag = 0;
	SIM_start();
	return flag;
}

/*
 * step run of SkyEye
 */
int com_si(char* arg){
	int flag = 0;
	char** endptr;
	int steps;
	if(arg == NULL || *arg == '\0') 
		steps = 1;
	else{
		errno = 0;
		steps = strtoul(arg, endptr, 10);
		#if 0
	/* if *nptr is not '\0' but **endptr is '\0', the entire string is valid */
		if(**endptr != '\0'){
			printf("Can not run the given steps.\n ");
			return flag;
		}
		#endif
		#if 0	
		if(errno != 0){
			if(errno == EINVAL)
				printf("Not valid digital format.\n");
			else if(errno == ERANGE)
				printf("Out of range for your step number.\n");
			else
				printf("Some unknown error in your stepi command.\n");
			return 1;
		}
		#endif
	}
	skyeye_stepi(steps);
	run_command("disassemble");
	return flag;
}

int com_x(char* arg){
	int flag = 0;
	char** endptr;
	char result[64];
	int addr, size;
	uint32 value;
	if(arg == NULL || *arg == '\0') 
		return Invarg_exp;
	else{
		flag = get_parameter(result, arg, "addr");
		if(flag > 0)
			addr = strtoul(result, NULL, 0);
		else{
			printf("Format error: x addr=xxxx size=xxxx\n");
			return -1;
		}
		flag = get_parameter(result, arg, "size");
		if(flag > 0)
			size = strtoul(result, NULL, 0);
		else{
			printf("size default setting 1 word\n");
			size = 1;
		}

		#if 0
		errno = 0;
		addr = strtoul(arg, endptr, 16);
	/* if *nptr is not '\0' but **endptr is '\0', the entire string is valid */
		if(**endptr != '\0'){
			printf("Can not run the given steps.\n ");
			return flag;
		}
		#endif
		#if 0	
		if(errno != 0){
			if(errno == EINVAL)
				printf("Not valid digital format.\n");
			else if(errno == ERANGE)
				printf("Out of range for memory address.\n");
			else
				printf("Some unknown error in your x command.\n");
			return 1;
		}
		#endif
	}
	int i;
	for(i = 1; i <= size; i++){
		bus_read(32, addr, &value);
		printf("0x%x:0x%x\t", addr, value);
		addr += 4;
		if(i%5 == 0)
			printf("\n");
	}
	printf("\n");

	return 0;
}


/* String to pass to system ().  This is for the LIST, VIEW and RENAME
   commands. */
static char syscom[1024];

/* List the file(s) named in arg. */
int com_list (arg)
     char *arg;
{
  if (!arg)
    arg = "";

  sprintf (syscom, "ls -FClg %s", arg);
  return (system (syscom));
}

int com_view (arg)
     char *arg;
{
  if (!valid_argument ("view", arg))
    return 1;

#if defined (__MSDOS__)
  /* more.com doesn't grok slashes in pathnames */
  sprintf (syscom, "less %s", arg);
#else
  sprintf (syscom, "more %s", arg);
#endif
  return (system (syscom));
}

int com_rename (arg)
     char *arg;
{
  too_dangerous ("rename");
  return (1);
}

int com_stat (arg)
     char *arg;
{
  struct stat finfo;

  if (!valid_argument ("stat", arg))
    return (1);

  if (stat (arg, &finfo) == -1)
    {
      perror (arg);
      return (1);
    }

  printf ("Statistics for `%s':\n", arg);

  printf ("%s has %d link%s, and is %d byte%s in length.\n",
	  arg,
          finfo.st_nlink,
          (finfo.st_nlink == 1) ? "" : "s",
          finfo.st_size,
          (finfo.st_size == 1) ? "" : "s");
  printf ("Inode Last Change at: %s", ctime (&finfo.st_ctime));
  printf ("      Last access at: %s", ctime (&finfo.st_atime));
  printf ("    Last modified at: %s", ctime (&finfo.st_mtime));
  return (0);
}

int com_delete (arg)
     char *arg;
{
  too_dangerous ("delete");
  return (1);
}


/* Change to the directory ARG. */
int com_cd (arg)
     char *arg;
{
  if (chdir (arg) == -1)
    {
      perror (arg);
      return 1;
    }

  com_pwd ("");
  return (0);
}

/* Print out the current working directory. */
int com_pwd (ignore)
     char *ignore;
{
  char dir[1024], *s;

  s = getcwd (dir, sizeof(dir) - 1);
  if (s == 0)
    {
      printf ("Error getting pwd: %s\n", dir);
      return 1;
    }

  printf ("Current directory is %s\n", dir);
  return 0;
}

extern void set_cli_done();
/* The user wishes to quit using this program.  Just set DONE non-zero. */
int com_quit (arg)
     char *arg;
{

  SIM_stop(NULL);
  set_cli_done();  
  SIM_quit();
  return 0;
}

int com_list_modules(char* arg){
	char* format = "%-20s\t%s\n";
	printf(format, "Module Name", "File Name");
#if 0
	skyeye_module_t* list = get_module_list();
	while(list != NULL){
		printf(format, list->module_name, list->filename);
		list = list->next;
	}
#endif
	return 0;
}

int com_list_options(char* arg){
	char* format = "%-20s\t%s\n";
	printf(format, "Option Name", "Description");
	skyeye_option_t* list = get_option_list();
	while(list != NULL){
		//printf("option_name = %s\n", list->option_name);
		printf(format, list->option_name, list->helper);
		list = list->next;
	}
	return 0;
}

int com_list_machines(char* arg){
	char* format = "%-20s\n";
	printf(format, "Machine Name");
	machine_config_t* list = get_mach_list();
	while(list != NULL){
		//printf("option_name = %s\n", list->option_name);
		printf(format, list->machine_name);
		list = list->next;
	}
	return 0;
}

int com_show_pref(char* arg){
	char* format = "%-30s\t%s\n";
	char* int_format = "%-30s\t0x%x\n";
	sky_pref_t * pref = get_skyeye_pref();
	printf(format, "Module search directorys:", pref->module_search_dir);
	printf(int_format, "Boot address:", pref->start_address);
	printf(format, "Executable file:", pref->exec_file);
	printf(int_format, "Load base for executable file:", pref->exec_load_base);
	printf(int_format, "Load mask for executable file:", pref->exec_load_mask);
	printf(format, "SkyEye config file:", pref->conf_filename);
	printf(format, "Endian of exec file:", pref->endian == Big_endian?"Big Endian":"Little endian");
	return 0;
}

int com_show_map(char* arg){
	mem_bank_t* bank;
	mem_config_t* memmap = get_global_memmap();
	char* format1 = "%-30s\t%s\t%20s\n";
	printf(format1, "Start Addr", "Length", "Type");
	char* format2 = "0x%-30x\t0x%x\t%20s\n";
	
	for (bank = memmap->mem_banks; bank->len; bank++)
		printf(format2, bank->addr, bank->addr + bank->len, 
			bank->type?"memory":"IO");
	return 0;
}

int com_load_module(char* arg){
	return 0;
}
int com_info(char* arg){
	char* info_target[] = {"registers", "breakpoint"};
	if(arg == NULL){
		printf("Available options is %s, %s\n", info_target[0], info_target[1]);
		return 0;
	}
#if 0
	if(!strncmp(arg,info_target[0], strlen(info_target[0]))){
	/* display the information of int register */
		if(arch_instance->get_regval_by_id){
			/* only for arm */		
			int i = 0;
			uint32 reg_value = 0;
			//while(arm_regstr[i]){
			while(i <= arch_instance->get_regnum()){
				reg_value = arch_instance->get_regval_by_id(i);
				printf("%s\t0x%x\n", arch_instance->get_regname_by_id(i), reg_value);
				i++;
			}
		}
		else{
			printf("Current processor not implement the interface.\n");
			return 0;
		}
	}
	else if(!strncmp(arg, info_target[1], strlen(info_target[1]))){
	/* display the information of current breakpoint */
	}
	else{
		printf("can not find information for the object %s\n", arg);
	}
#endif
	return 0;
}
void set_entry_to_cpu(conf_object_t *core, uint32_t entry_point)
{
	attr_value_t value;
	if(!SKY_has_attribute(core, "elf_entry_point")){
		value = SKY_make_attr_uinteger(entry_point);
		SKY_set_attribute(core, "elf_entry_point", &value);
	}
	return ;
 }


int32_t WIN_load_binary(char *cpuname, char *elfname){
	generic_address_t start_addr;
	exception_t exp;
	conf_object_t *core = get_conf_obj(cpuname);
	if(core == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get obj for %s.\n", cpuname);
		return Error;
	}
	if(is_elf(elfname)){
		exp = get_elf_start_addr(elfname, &start_addr);
	}else if(is_coff(elfname)){
		exp = get_coff_start_addr(elfname, &start_addr);
	}else if(is_hex(elfname)){
		exp = get_hex_start_addr(elfname, &start_addr);

	}else{
		exp = Invarg_exp;
	}
	if(!SKY_has_attribute(core, "load_mask")){
		attr_value_t attr = SKY_get_attribute(core, "load_mask");
		uint32_t load_mask = 0;
		load_mask = SKY_attr_uinteger(attr);
		if (load_mask != 0)
			start_addr = (start_addr & load_mask);
	}
	if(!SKY_has_attribute(core, "load_addr")){
		attr_value_t attr = SKY_get_attribute(core, "load_addr");
		uint32_t load_addr = 0;
		load_addr = SKY_attr_uinteger(attr);
		if (load_addr != 0)
			start_addr = start_addr | load_addr;
	}

	set_entry_to_cpu(core, start_addr);
	SIM_set_pc(core, start_addr);
	SKY_load_file(core, NULL, elfname);
	if(exp != No_exp){
		skyeye_log(Error_log, __FUNCTION__, "File format not recognized\n");
		return -1;
	}
}

int prepare_to_run(void){
	int i, j;
	conf_object_t *cpu;
	sky_pref_t* pref = get_skyeye_pref();
	system_clock_calculation();

	skyeye_system_t *system = system_get();
	for(i = 0; i < system->soc_cnt; i++){
		sys_soc_t *soc = system->socs + i;
		sys_cpu_t *sys_cpu;
		skyeye_machine_intf *inface = (skyeye_machine_intf *)SKY_get_iface(soc->soc, MACHINE_INTF_NAME);
		if(inface && inface->update_boot_info)
			inface->update_boot_info(soc->soc);

		for(j = 0; j < soc->cpu_cnt; j++)
		{
			sys_cpu = &(soc->cpus[j]);

			if(pref->systemc_enable == True){	
				systemc_exec_t *exec = create_systemc_exec();
				exec->priv_data = sys_cpu;
				exec->run = system_cpu_cycle;
				exec->stop = NULL;
				add_to_exec_list(exec);

			}else{
				skyeye_exec_t* exec = create_exec();
				exec->priv_data = sys_cpu;
				exec->run = system_cpu_cycle;
				exec->stop = NULL;
				skyeye_cell_t* cell = create_cell();
				add_to_cell(exec, cell);
			}
		}
	}
	SKY_before_init_hap_occurred();
	return 0;
}

int com_load_conf(char* arg){
#if 0
	int ret;
	sky_pref_t *pref = get_skyeye_pref();
	char *filename = basename(arg);
	skyeye_config_t* config = get_current_config();
	config->running_mode = GUIMODE;
	if(!strcmp(filename, JSON_CONF_NAME)){
		ret = json_conf(JSON_CONF_NAME, NULL);
		if(ret == -1){
			printf("Can not parse the config file  %s correctly.\n ", arg);
		}
		return ret;
	}
	if ((ret = skyeye_read_config(arg)) != No_exp){
		if(ret == Conf_format_exp){
			printf("Can not parse the config file  %s correctly.\n ", arg);
		}
		if(ret == File_open_exp)
			printf("Can not open the config file %s\n", arg);
		else
			printf("Unknown error when read config from the file %s\n", arg);
	}
	pref->conf_filename = NULL;

#endif
	return 0;
}


int com_reset(char* arg){
	//generic_arch_t* arch_instance = get_arch_instance("");
	skyeye_config_t* config = get_current_config();
	/* get the current preference for simulator */
	sky_pref_t *pref = get_skyeye_pref();
	/* reset current arch_instanc */
	
	//arch_instance->reset();
	printf("reset not implemented.\n");
	/* reset all the values of mach */
	config->mach->mach_io_reset(NULL);
	generic_address_t pc = (config->start_address & pref->exec_load_mask)|pref->exec_load_base;
	skyeye_log(Info_log, __FUNCTION__, "Set PC to the address 0x%x\n", pc);
	//arch_instance->set_pc(pc);

	return 0;
}

void SIM_restart(void);

int com_restart(char* arg){
	SIM_restart();
	return 0;
}

int com_set_all(char* arg)
{
	char* info_target[] = {"reg", "addr"};
	char result[64];
	int i = 0;
	if(arg == NULL){
		printf("Available options is %s, %s\n", info_target[0], info_target[1]);
		return 0;
	}
	for(i = 0; i < 2; i++){
		if(!strncmp(arg,info_target[i], strlen(info_target[i]))){
			break;
		}
	}
	//generic_arch_t* arch_instance = get_arch_instance(NULL);
	skyeye_reg_intf* reg_intf = SIM_get_current_reg_intf();
	switch(i){
	case 0:
		i = get_parameter(result, arg, "reg");
		char* reg_name[64];
		int reg_value;
		if(i > 0){
			strcpy(reg_name, result);
			//printf("set reg = %s\t", reg_name);
		}
		i = get_parameter(result, arg, "value");
		if(i > 0){
			reg_value = strtoul(result, NULL, 0);
			//printf("value = 0x%x\n", reg_value);
		}
		uint32 reg_id = reg_intf->get_regid_by_name(reg_intf->conf_obj, reg_name);
		reg_intf->set_regvalue_by_id(reg_intf->conf_obj, reg_value, reg_id);
		break;
	case 1:
		i = get_parameter(result, arg, "addr");
		int addr, value, size;
		if(i > 0){
			addr = strtoul(result, NULL, 0);
			//printf("set addr = 0x%x\t", addr);
		}else{
			printf("Format error: set addr = xxxx value = xxxxx size = xxxx\n");
			return -1;
		}
		i = get_parameter(result, arg, "value");
		if(i > 0){
			value = strtoul(result, NULL, 0);
			//printf("value = 0x%x\t", value);
		}else{
			printf("Format error: set addr = xxxx value = xxxxx size = xxxx\n");
			return -1;
		}
		i = get_parameter(result, arg, "size");
		if(i > 0){
			size = strtoul(result, NULL, 0);
			//printf("size = 0x%x\n", size);
		}else{
			printf("default setting size = 4\n");
			size = 4;
		}
		switch(size){
			case 1:
				bus_write(8, addr, value);
				break;
			case 2:
				bus_write(16, addr, value);
				break;
			case 4:
				bus_write(32, addr, value);
				break;
			default:
				printf("size value should be 1 , 2, 4!\n");
				break;
		}
		break;
	default:
		printf("set command parameter error\n");
		return -1;
	}
	return 0;
}


/*This function is allow gui to call*/

uint32 gui_get_memory_value_by_cpuname(char *cpuname, char *addr){
	uint32 value, addr_value;
	exception_t ret;
	if(addr == NULL || addr == '\0') 
		return Invarg_exp;
	else{
		addr_value = strtoul(addr, NULL, 0);
	}


	conf_object_t *core = get_conf_obj(cpuname);
	//addr_space_t *space = (addr_space_t*)SKY_get_interface(core, ADDR_SPACE_INTF_NAME);
	addr_space_t* space = get_space_from_attr(core);
	if(space != NULL){
		ret = space->memory_space->read(core, addr_value, &value, 4);
	}

	//bus_read(32, addr_value, &value);
	return value;
}

char *gui_get_cpu_register(char *cpuname){
	char temp[500];
	int num, i = 0;
	if(tempPoint != NULL){
		free(tempPoint);
		tempPoint = NULL;
	}
	char *register_p = skyeye_mm(sizeof(char));
	num = 1;
	*register_p = '\0';
	uint32 reg_value = 0;
	conf_object_t *core = get_conf_obj(cpuname);
	if(core == NULL)
		return NULL;
	skyeye_reg_intf* inface =  (skyeye_reg_intf*)SKY_get_iface(core, SKYEYE_REG_INTF);
	if(inface == NULL)
		return NULL;
	while(i < inface->get_regnum(inface->conf_obj)){
		reg_value = inface->get_regvalue_by_id(core, i);
			
		sprintf(temp, "%s:%x;", inface->get_regname_by_id(inface->conf_obj, i), reg_value);
		i++;
		num = num + strlen(temp) + 1;
		register_p = realloc(register_p, sizeof(char) * num);
		strcat(register_p, temp);
	}
	num = num + strlen("|");
	register_p = realloc(register_p, sizeof(char) * num);
	strcat(register_p, "|");
	tempPoint = register_p;
	return register_p;
}

char *gui_info_register(void)
{
	char temp[500];
	int num = 1;
	char *register_p = NULL;
	register_p = skyeye_mm(sizeof(char) * 1);
	*register_p = '\0'; 
	/* generic_arch_t* arch_instance = get_arch_instance("");
	if(arch_instance == NULL)
		return NULL;
	*/
	skyeye_reg_intf* reg_intf = SIM_get_current_reg_intf();
	if(reg_intf->get_regvalue_by_id){
		int i = 0;
		uint32 reg_value = 0;
		while(i < reg_intf->get_regnum(reg_intf->conf_obj) && reg_intf->get_regname_by_id(reg_intf->conf_obj, i)){
			reg_value = reg_intf->get_regvalue_by_id(reg_intf->conf_obj, i);
			sprintf(temp, "%s:%x;", reg_intf->get_regname_by_id(reg_intf->conf_obj, i), reg_value);
			i++;
			num = num + strlen(temp) + 1;
			register_p = realloc(register_p, sizeof(char) * num);
			strcat(register_p, temp);
		}
		num = num + strlen("|");
		register_p = realloc(register_p, sizeof(char) * num);
		strcat(register_p, "|");
		return register_p;

	}
	else{
		printf("Current processor not implement the interface.\n");
		return NULL;
	}

	return NULL;
}

char *gui_get_current_mach(void)
{
	machine_config_t *current_mach = get_current_mach();
	skyeye_config_t* config = get_current_config();
	if(config->machs.soc_num > 0){
		return config->machs.soc[0]->mach->class_name;
	}
	if(current_mach == NULL)
		return NULL;
	return  current_mach->machine_name;
}


char *gui_get_device_list(void)
{
	int i;
	char temp[500];
	int num = 1;
	char *devicelist_p = NULL;
	if(tempPoint != NULL){
		free(devicelist_p);
		tempPoint = NULL;
	}
	devicelist_p = skyeye_mm(sizeof(char) * 1);
	*devicelist_p = '\0';
	machine_config_t *current_mach = get_current_mach();
	if(current_mach->phys_mem != NULL){
		map_info_t **maparray = current_mach->phys_mem->map_array;
		for(i = 0; maparray[i] != NULL; i++){
			sprintf(temp, "%s;", maparray[i]->memory_space->conf_obj->objname);
			num = num + strlen(temp) + 1;
			devicelist_p = realloc(devicelist_p, sizeof(char) * num);
			strcat(devicelist_p, temp);
		}
		num = num + strlen("|");
		devicelist_p = realloc(devicelist_p, sizeof(char) * num);
		strcat(devicelist_p, "|");
		tempPoint = devicelist_p;
		return devicelist_p;
	}else{
		skyeye_free(devicelist_p);
	}
	return NULL;
}

uint64_t gui_get_step(void)
{
	uint64_t step;
	
	step = SIM_get_current_steps();
	return step;

}


void gui_reset_mach(void)
{
	int  i, mach_count;
	skyeye_config_t* config = get_current_config();
	sky_pref_t *pref = get_skyeye_pref();
#if 0
	mach_count = config->machs.soc_num;	
	for(i =0; i < mach_count; i++){
		conf_object_t *class_obj = NULL;
		skyeye_class_t *class_data = NULL;
		class_obj = get_conf_obj(config->machs.soc[i]->mach->class_name);
		class_data = class_obj->obj;
		if(class_data->reset_instance)
			class_data->reset_instance(config->machs.soc[i]->mach, NULL);
	}
//#else
		conf_object_t *class_obj = NULL;
		skyeye_class_t *class_data = NULL;
		if(config->mach_obj){
			class_data = config->mach_obj->class_data;
			printf("calssdata:%x\n", class_data);
			printf("%d\n", __LINE__);
			if(class_data->reset_instance){
				printf("machname:%s\n", config->mach_obj->objname);
				printf("%d\n", __LINE__);
				class_data->reset_instance(config->mach_obj, NULL);
				printf("%d\n", __LINE__);
			}
		}
#endif
	generic_address_t pc = (config->start_address & pref->exec_load_mask)|pref->exec_load_base;
	skyeye_log(Info_log, __FUNCTION__, "Set PC to the address 0x%x\n", pc);
	//arch_instance->set_pc(pc);
}

int gui_get_cpu_rate(void)
{
	int i;
	skyeye_config_t* config = get_current_config();
	return config->cpu_rate;
}


uint32_t gui_get_pc_by_cpuname(char *cpuname){
	conf_object_t * core = get_conf_obj(cpuname);

	return SIM_get_pc(core);
}

int gui_stepi(char *cpuname, char* arg){
	char** endptr;
	int steps, ret = 0;
	conf_object_t *core = get_conf_obj(cpuname);
	if(core == NULL)
		return 0;
	if(arg == NULL || *arg == '\0') 
		steps = 1;
	else{
		errno = 0;
		steps = strtoul(arg, endptr, 10);
	}

	if(SIM_get_core_mode(core) == DYNCOM_RUN) {
		SIM_stop(NULL);
		//TODO(wshen): can't stop the simulator thread competely
		usleep(50000);
		SIM_set_core_mode(core, DYNCOM_RUN_SINGLE_STEP);	
		printf("Waiting for the quiescence of the System...\n");
	}

	skyeye_core_stepi(core, steps);
	return ret;
}

int gui_create_breakpoint(char *cpuname, uint32_t addr){
#if 0
	conf_object_t *core = get_conf_obj(cpuname);
	//addr_space_t *space = (addr_space_t*)SKY_get_iface(core, ADDR_SPACE_INTF_NAME);
	addr_space_t* space = get_space_from_attr(core);
	breakpoint_mgt_t *break_manager = &space->bp_manager;
	ret = create_a_breakpoint(break_manager, addr);
	if(ret != 0)
		return 0;
	return 1;
#else
	int ret;
	conf_object_t *core = get_conf_obj(cpuname);
	if(!core)
		return Error;
	core_breakpoint_op_intf *iface = SKY_get_iface(core, CORE_BREAKPOINT_OP_INTF_NAME);
	if(!iface){
		skyeye_log(Warning_log, __FUNCTION__, "can not get %s interface from %s\n", CORE_BREAKPOINT_OP_INTF_NAME, cpuname);
		return Error;
	}
	ret = iface->insert_bp(core, addr);
	if(ret != 0){
		return Error;
	}
	return Ok;
#endif
}

int gui_delete_breakpoint_by_id(char *cpuname, int id){
	uint32_t address;
#if 0
	conf_object_t *core = get_conf_obj(cpuname);
	//addr_space_t *space = (addr_space_t*)SKY_get_iface(core, ADDR_SPACE_INTF_NAME);
	addr_space_t* space = get_space_from_attr(core);
	breakpoint_mgt_t *break_manager = &space->bp_manager;
	ret = remove_bp(break_manager, id);
	if(ret != 0)
		return 1;
	return 0;
#else
	conf_object_t *core = get_conf_obj(cpuname);
	if(!core)
		return Error;
	core_breakpoint_op_intf *iface = SKY_get_iface(core, CORE_BREAKPOINT_OP_INTF_NAME);
	if(!iface){
		skyeye_log(Warning_log, __FUNCTION__, "can not get %s interface from %s\n", CORE_BREAKPOINT_OP_INTF_NAME, cpuname);
		return Error;
	}
	address = iface->get_bp_addr_by_id(core, id);
	iface->delete_bp(core, address);
	return Ok;
#endif
}

int gui_delete_breakpoint_by_addr(char *cpuname, uint32_t address){
	
	conf_object_t *core = get_conf_obj(cpuname);
	if(!core)
		return Error;
	core_breakpoint_op_intf *iface = SKY_get_iface(core, CORE_BREAKPOINT_OP_INTF_NAME);
	if(!iface){
		skyeye_log(Warning_log, __FUNCTION__, "can not get %s interface from %s\n", CORE_BREAKPOINT_OP_INTF_NAME, cpuname);
		return Error;
	}
	iface->delete_bp(core, address);
	return Ok;
}

uint32_t gui_get_breakpoint_address_by_id(char *cpuname, int id)
{
#if 0
	conf_object_t *core = get_conf_obj(cpuname);
	//addr_space_t *space = (addr_space_t*)SKY_get_interface(core, ADDR_SPACE_INTF_NAME);
	addr_space_t* space = get_space_from_attr(core);
	breakpoint_mgt_t *break_manager = &space->bp_manager;
	breakpoint_t *bp = get_bp_by_id(break_manager, id);
	return bp->address;
#else
	conf_object_t *core = get_conf_obj(cpuname);
	if(!core)
		return Error;
	core_breakpoint_op_intf *iface = SKY_get_iface(core, CORE_BREAKPOINT_OP_INTF_NAME);
	if(!iface){
		skyeye_log(Warning_log, __FUNCTION__, "can not get %s interface from %s\n", CORE_BREAKPOINT_OP_INTF_NAME, cpuname);
		return Error;
	}
	return iface->get_bp_addr_by_id(core, id);

#endif
}


int gui_get_bp_numbers(char *cpuname)
{
#if 0
	conf_object_t *core = get_conf_obj(cpuname);
	//addr_space_t *space = (addr_space_t*)SKY_get_interface(core, ADDR_SPACE_INTF_NAME);
	addr_space_t* space = get_space_from_attr(core);
	breakpoint_mgt_t *break_manager = &space->bp_manager;
	return  get_bp_numbers(break_manager);
#else
	conf_object_t *core = get_conf_obj(cpuname);
	if(!core)
		return Error;
	core_breakpoint_op_intf *iface = SKY_get_iface(core, CORE_BREAKPOINT_OP_INTF_NAME);
	if(!iface){
		skyeye_log(Warning_log, __FUNCTION__, "can not get %s interface from %s\n", CORE_BREAKPOINT_OP_INTF_NAME, cpuname);
		return Error;
	}
	return iface->get_bp_numbers(core);
#endif
}

int gui_check_bp_hit(char *cpuname)
{
#if 0
	conf_object_t *core = get_conf_obj(cpuname);
	//addr_space_t *space = (addr_space_t*)SKY_get_interface(core, ADDR_SPACE_INTF_NAME);
	addr_space_t* space = get_space_from_attr(core);
	breakpoint_mgt_t *break_manager = &space->bp_manager;
	int ret = check_breakpoint_hit_flag(break_manager);
	if(ret == 0)
		return 1;

	return 0;
#else
	conf_object_t *core = get_conf_obj(cpuname);
	if(!core)
		return Error;
	core_breakpoint_op_intf *iface = SKY_get_iface(core, CORE_BREAKPOINT_OP_INTF_NAME);
	if(!iface){
		skyeye_log(Warning_log, __FUNCTION__, "can not get %s interface from %s\n", CORE_BREAKPOINT_OP_INTF_NAME, cpuname);
		return Error;
	}
	return iface->check_bp_trigger(core);
	
#endif
}

int gui_get_bp_hit_id(char *cpuname)
{
#if 0
	conf_object_t *core = get_conf_obj(cpuname);
	//addr_space_t *space = (addr_space_t*)SKY_get_interface(core, ADDR_SPACE_INTF_NAME);
	addr_space_t* space = get_space_from_attr(core);
	breakpoint_mgt_t *break_manager = &space->bp_manager;
	int id = get_breakpoint_hit_id(break_manager);
	if(id != -1)
		return id;

	return 0;
#else
	conf_object_t *core = get_conf_obj(cpuname);
	if(!core)
		return Error;
	core_breakpoint_op_intf *iface = SKY_get_iface(core, CORE_BREAKPOINT_OP_INTF_NAME);
	if(!iface){
		skyeye_log(Warning_log, __FUNCTION__, "can not get %s interface from %s\n", CORE_BREAKPOINT_OP_INTF_NAME, cpuname);
		return Error;
	}
	return iface->get_bp_trigger(core);

#endif
}


int gui_clear_bp_hit(char *cpuname)
{
#if 0
	conf_object_t *core = get_conf_obj(cpuname);
	//addr_space_t *space = (addr_space_t*)SKY_get_interface(core, ADDR_SPACE_INTF_NAME);
	addr_space_t* space = get_space_from_attr(core);
	breakpoint_mgt_t *break_manager = &space->bp_manager;
	clear_breakpoint_hit_flag(break_manager);
	return 0;
#else
	int ret;
	conf_object_t *core = get_conf_obj(cpuname);
	if(!core)
		return Error;
	core_breakpoint_op_intf *iface = SKY_get_iface(core, CORE_BREAKPOINT_OP_INTF_NAME);
	if(!iface){
		skyeye_log(Warning_log, __FUNCTION__, "can not get %s interface from %s\n", CORE_BREAKPOINT_OP_INTF_NAME, cpuname);
		return Error;
	}
	
	ret = iface->clear_bp_trigger(core);
	if(ret != 0)
		return Error;
	return Ok;
#endif
}


int gui_create_remote_gdb(char *cpuname, int port, char *ip)
{
	char objname[MAX_OBJNAME];
	if(strlen(cpuname) + strlen("_gdbserver") + 1 > MAX_OBJNAME){
		return 0;
	}
	get_strcat_objname(objname, cpuname, "_gdbserver");
	conf_object_t * gdbserver_obj = pre_conf_obj(objname, "gdbserver");
	if(gdbserver_obj == NULL){
		return 0;
	}

	skyeye_gdbserver_intf *inface = (skyeye_gdbserver_intf *)SKY_get_iface(gdbserver_obj, SKYEYE_GDBSERVER_INTF_NAME);
	int ret = inface->GdbServerStart(gdbserver_obj, cpuname, port, ip);
	if(ret == GDB_SERVER_ERROR){
		free_conf_obj(gdbserver_obj);
		return 0;
	}

	return 1;
}
int gui_remote_gdb_check_link(char *cpuname){
	char objname[MAX_OBJNAME];
	if(strlen(cpuname) + strlen("_gdbserver") + 1 > MAX_OBJNAME){
		return 0;
	}
	get_strcat_objname(objname, cpuname, "_gdbserver");
	conf_object_t *obj = get_conf_obj(objname);
	if(obj == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not find obj [%s]\n", objname);
		return 0;
	}
	skyeye_gdbserver_intf *inface = (skyeye_gdbserver_intf *)SKY_get_iface(obj, SKYEYE_GDBSERVER_INTF_NAME);
	int ret = inface->GdbServerIsConnected(obj);
	return ret;
}

char *gui_remote_gdb_get_client_ip(char *cpuname){
	char objname[MAX_OBJNAME];
	if(strlen(cpuname) + strlen("_gdbserver") + 1 > MAX_OBJNAME){
		return 0;
	}
	get_strcat_objname(objname, cpuname, "_gdbserver");
	conf_object_t *obj = get_conf_obj(objname);
	if(obj == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not find obj [%s]\n", objname);
		return 0;
	}
	skyeye_gdbserver_intf *inface = (skyeye_gdbserver_intf *)SKY_get_iface(obj, SKYEYE_GDBSERVER_INTF_NAME);
	char *ip = inface->GdbServerGetClientIp(obj);
	return ip;
}

int gui_delete_remote_gdb(char *cpuname){
	char objname[MAX_OBJNAME];
	if(strlen(cpuname) + strlen("_gdbserver") + 1 > MAX_OBJNAME){
		return 0;
	}
	get_strcat_objname(objname, cpuname, "_gdbserver");
	conf_object_t *obj = get_conf_obj(objname);
	if(obj == NULL){
		skyeye_log(Warning_log, __FUNCTION__, "Free obj [%s] fail\n", objname);
		return 0;
	}
	free_conf_obj(obj);
	return 1;
}


int WIN_create_mach(char *objname, char *classname){
	skyeye_config_t* config = get_current_config();	

	conf_object_t * conf_obj = pre_conf_obj(objname, classname);
	if(conf_obj == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Create %s object fail.", objname);
		return Error;
	}
	system_register_soc(conf_obj);
	return Ok;
}

int WIN_create_linker(char *objname, char *classname){
	conf_object_t * conf_obj = pre_conf_obj(objname, classname);
	if(conf_obj == NULL)
		return Error;
	system_register_linker(conf_obj);
	return Ok;
}

int WIN_create_cpu(char *machname ,char *objname, char *classname){
	conf_object_t* cpu = pre_conf_obj(objname, classname);
	if(cpu == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get obj for %s.\n", objname);
		return Error; /* FIXME, error handler */
	}
	core_update_memiface_cache *iface = SKY_get_iface(cpu, CORE_UPDATE_MEMIFACE_CACHE_NAME);
	if(iface != NULL){
		iface->update_memiface_cache(cpu);
	}
	exception_t ret = reset_conf_obj(cpu);
	conf_object_t *mach_obj = get_conf_obj(machname);
	skyeye_machine_intf *inface = (skyeye_machine_intf *)SKY_get_iface(mach_obj, MACHINE_INTF_NAME);

	addr_space_t *space = get_space_from_attr(cpu);
	if(space){
		inface->set_space(mach_obj, space);
		inface->set_core(mach_obj, cpu);
	}
	sys_cpu_t *sys_cpu = system_register_cpu(cpu, mach_obj);
	sys_cpu->core_exec = (core_exec_intf*)SKY_get_iface(cpu, CORE_EXEC_INTF_NAME);

	return Ok;
}

int WIN_create_device(char *machname ,char *objname, char *classname){
	skyeye_config_t* config = get_current_config();	
	conf_object_t *mach_obj = get_conf_obj(machname);
	conf_object_t* device = pre_conf_obj(objname, classname);
	if(device == NULL)
		return Error;
	reset_conf_obj(device);
	conf_object_t *core ;
	if (strcmp(classname, "ram") == 0){
		attr_value_t *attr_after_making;
		sys_soc_t *sys_soc =  mach_obj->sys_struct;
		if(sys_soc->cpu_cnt == 1){
			core = sys_soc->cpus[0].cpu;
			core_info_intf* core_info = (core_info_intf*)SKY_get_iface(core, CORE_INFO_INTF_NAME);
			endian_t endianess = core_info->get_endian(core);
			attr_after_making = make_new_attr(Val_UInteger, (void *)endianess);
			set_conf_attr(device, "endian", attr_after_making);

		}else{
			skyeye_log(Error_log, __FUNCTION__, "to manany cores in soc\n");
			//FIX ME
		}
	} 
	system_register_dev(device, mach_obj);
	return Ok;
}

void * WIN_pre_conf_obj(char *objname, char *classname){
	conf_object_t * conf_obj = pre_conf_obj(objname, classname);
	if(conf_obj == NULL)
		return Error;
	return conf_obj;	
}

int WIN_set_attr(char *objname, char *key, char *attr_type, char *value){
	skyeye_config_t* config = get_current_config();
	attr_value_t *attr_after_making;
	uint32_t attr_integer, base;
	char parity_val;
	if(!strncmp(attr_type, "string", strlen("string"))){
			attr_after_making = make_new_attr(Val_String, value);
			if(!strncmp(key, "parity", strlen("parity"))){
				attr_after_making = make_new_attr(Val_Integer, value[0]);
			}
			if(!strncmp(key, "elf", strlen("elf"))){
				conf_object_t *soc = get_conf_obj(objname);                                  
				register_soc_elfname(soc, value);
				return 0;
			}if(!strncmp(key, "brate", strlen("brate"))){
				return 0;
			}
	}else if(!strncmp(attr_type, "uinteger", strlen("uinteger"))){
			if(!strncmp(value, "0x", 2)){
				base = 16;
			}else{
				base = 10;
			}
			if(!strncmp(key, "brate", strlen("brate"))){
				return 0;
			}
			attr_integer  =  strtoll(value, NULL, base);
			attr_after_making = make_new_attr(Val_UInteger, attr_integer);
	}else if(!strncmp(attr_type, "integer", strlen("integer"))){
	}else if(!strncmp(attr_type, "floating", strlen("floating"))){
		attr_after_making = make_new_attr(Val_Floating, value);
	}else if(!strncmp(attr_type, "list", strlen("list"))){
	}else if(!strncmp(attr_type, "data", strlen("data"))){
	}else if(!strncmp(attr_type, "nil", strlen("nil"))){
	}else if(!strncmp(attr_type, "object", strlen("object"))){
		conf_object_t *obj = get_conf_obj(value);
		attr_after_making = make_new_attr(Val_Object, obj);
	}else if(!strncmp(attr_type, "dict", strlen("dict"))){
	}else if(!strncmp(attr_type, "boolean", strlen("boolean"))){
	}else if(!strncmp(attr_type, "ptr", strlen("ptr"))){
	}else{
		skyeye_log(Error_log, __FUNCTION__, "Unsupport attr type: %s\n", attr_type);
		exit(0);
	}

	conf_object_t *device = get_conf_obj(objname);
	set_conf_attr(device, key, attr_after_making);
	return 1;
}

int WIN_add_map(char *machname, char *devicename, generic_address_t base_addr, generic_address_t length){
	conf_object_t *mach_obj = get_conf_obj(machname);
	conf_object_t* device = get_conf_obj(devicename);
	memory_space_intf* io_memory = (memory_space_intf*)SKY_get_iface(device, MEMORY_SPACE_INTF_NAME);
	skyeye_machine_intf *inface = (skyeye_machine_intf *)SKY_get_iface(mach_obj, MACHINE_INTF_NAME);
	addr_space_t *space = inface->get_space(mach_obj);
	SKY_add_map(space, device, base_addr, length, 0x0, io_memory, 1, 1);
	return 1;
}


char *WIN_module_get_value_by_name(char *modulename, char *key)
{
	char *value = NULL;
	//value = SKY_module_get_value_by_name(modulename, key);
	return value;
}


char *WIN_module_get_path_by_name(char *modulename)
{
	char *value = NULL;
	//value = SKY_module_get_path_by_name(modulename);
	return value;
}

char *WIN_get_module_names(void){

	char *buf[512] = {'\0'};
	char *module_name = NULL;
#if 0
	int num = 1;
	skyeye_module_t *modules = get_module_list();
	if(tempPoint != NULL){
		free(tempPoint);
		tempPoint = NULL;
	}
	module_name = skyeye_mm(sizeof(char) * 1);
	*module_name = '\0';
	while(modules != NULL){
		sprintf(buf, "%s;", modules->module_name);		
		num = num + strlen(buf);
		module_name = realloc(module_name, sizeof(char) * num);
		strcat(module_name, buf);
		
		modules = modules->next;
	}
	num = num + strlen("|");
	module_name = realloc(module_name, sizeof(char) * num);
	strcat(module_name, "|");
	tempPoint = module_name;
#endif
	return module_name;
}

void WIN_load_module_fromdir(char *dir)
{
	//SKY_load_all_modules(dir, NULL);
}

void WIN_unload_all_modules(void){
	//SKY_unload_all_modules();
}




int WIN_load_file(char *cpuname, const char* filename, generic_address_t load_addr){
	int ret;
	conf_object_t *core = get_conf_obj(cpuname);
	if(core == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not find obj [%s]\n", cpuname);
	}
	ret = load_file(core, filename, load_addr);
	if(ret != No_exp){
		return 0;
	}
	return 1;
}


uint32_t WIN_get_dev_register_num(char *devicename)
{
	uint32_t reg_num;
	conf_object_t *obj = get_conf_obj(devicename);
	if(obj == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get obj for %s.\n", devicename);
		return -1;
	}
	skyeye_reg_intf *iface = SKY_get_iface(obj, SKYEYE_REG_INTF);
	if(iface == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get %s interface for %s.\n", SKYEYE_REG_INTF, devicename);
		return -1;
	}
	reg_num = iface->get_regnum(obj);
	return reg_num;
}


uint32_t WIN_get_regvalue_by_id(char *devicename, uint32_t id){
	conf_object_t *obj = get_conf_obj(devicename);
	if(obj == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get obj for %s.\n", devicename);
		return 0;
	}
	skyeye_reg_intf *iface = SKY_get_iface(obj, SKYEYE_REG_INTF);
	if(iface == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get %s interface for %s.\n", SKYEYE_REG_INTF, devicename);
		return 0;
	}
	return iface->get_regvalue_by_id(obj, id);
}


uint32_t WIN_get_regoffset_by_id(char *devicename, uint32_t id){
	conf_object_t *obj = get_conf_obj(devicename);
	if(obj == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get obj for %s.\n", devicename);
		return 0;
	}
	skyeye_reg_intf *iface = SKY_get_iface(obj, SKYEYE_REG_INTF);
	if(iface == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get %s interface for %s.\n", SKYEYE_REG_INTF, devicename);
		return 0;
	}
	if(iface->get_regoffset_by_id == NULL)
		return 0xffffffff;
	return iface->get_regoffset_by_id(obj, id);
}

char *WIN_get_regname_by_id(char *devicename, uint32_t id){
	conf_object_t *obj = get_conf_obj(devicename);
	if(obj == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get obj for %s.\n", devicename);
		return 0;
	}
	skyeye_reg_intf *iface = SKY_get_iface(obj, SKYEYE_REG_INTF);
	if(iface == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get %s interface for %s.\n", SKYEYE_REG_INTF, devicename);
		return -1;
	}
	return iface->get_regname_by_id(obj, id);
}

uint32_t WIN_set_regvalue_by_id(char *devicename, uint32_t value, uint32_t id){
	conf_object_t *obj = get_conf_obj(devicename);
	if(obj == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get obj for %s.\n", devicename);
		return 0;
	}
	skyeye_reg_intf *iface = SKY_get_iface(obj, SKYEYE_REG_INTF);
	if(iface == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get %s interface for %s.\n", SKYEYE_REG_INTF, devicename);
		return 0;
	}
	return iface->set_regvalue_by_id(obj, value, id);
}

uint32_t WIN_get_regid_by_name(char *devicename, char* name){
	conf_object_t *obj = get_conf_obj(devicename);
	if(obj == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get obj for %s.\n", devicename);
		return 0;
	}
	skyeye_reg_intf *iface = SKY_get_iface(obj, SKYEYE_REG_INTF);
	if(iface == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get %s interface for %s.\n", SKYEYE_REG_INTF, devicename);
		return 0;
	}
	return iface->get_regid_by_name(obj, name);
}

char *WIN_disassemble(char *cpuname, uint32_t addr){
	conf_object_t *obj = get_conf_obj(cpuname);
	if(obj == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get obj for %s.\n", cpuname);
		return 0;
	}
	core_info_intf *iface = SKY_get_iface(obj, CORE_INFO_INTF_NAME);
	if(iface == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get %s interface for %s.\n", CORE_INFO_INTF_NAME);
		return 0;
	}
	return iface->disassemble(obj, addr);
}




char **get_class_list(void){
	return SKY_get_classname_list();
}

char *get_class_type(char *classname){
	conf_class_t *cls = SKY_get_class(classname);
	return cls->class_data.class_type;
	
}
char **get_interface_list(char *classname)
{
	conf_class_t *cls = SKY_get_class(classname);
	if(cls == NULL)
		return  NULL;
	return class_get_iface_list(cls);
}

char **get_connect_list(char *classname)
{
	static char *cache[128];
	conf_class_t *cls = SKY_get_class(classname);
	if(cls == NULL)
		return  NULL;
	return class_get_connect_list(cls);
}

char **get_attr_list(char *classname)
{
	conf_class_t *cls = SKY_get_class(classname);
	if(cls == NULL)
		return  NULL;
	return get_class_attributes(cls);
}

char **get_attr_info(char *classname, char *attrname){
	conf_class_t *cls = SKY_get_class(classname);
	if(cls == NULL)
		return  NULL;
	return get_class_attrinfo(cls, attrname);
}



int connect_interface(char *pro, char *iface_name, char *rec, char *iface_attr_name){
	return 0;
}

int load_module_fromdir(char *dir){
	//init_module_list();
	//SKY_load_all_modules(dir, NULL);
	return 0;
}

int load_module_fromws(char *ws_dir){
#define MAX_LEN 256
#define MAX_DIR 64
    char module_dir[MAX_LEN]; 
    struct dirent *direntp;
    struct stat st;
    char path[MAX_LEN];
    char *dirs[MAX_DIR] = {NULL};
    int index = 0;
    DIR * dir = NULL;
    if(strlen(ws_dir) + 1 > MAX_LEN)
		skyeye_log(Error_log, __FUNCTION__, "dirname is too long.\n");
    sprintf(module_dir, "%s\\%s",ws_dir, "targets\\skyeye\\");
    dir = opendir(module_dir);
    if (dir == NULL)
		skyeye_log(Error_log, __FUNCTION__, "Can not open dir:%s.\n", module_dir);
    dirs[index] = malloc(strlen(module_dir) + 1);
    if(strlen(module_dir) + 1 > MAX_LEN)
		skyeye_log(Error_log, __FUNCTION__, "dirname is too long.\n");
    sprintf(dirs[index], "%s", module_dir);
    index++;
    while ((direntp = readdir(dir)) != NULL){
	if(strncmp(direntp->d_name, ".", 1) == 0)
		continue;
        if(strlen(module_dir) + strlen(direntp->d_name) + 1 > MAX_LEN)
		skyeye_log(Error_log, __FUNCTION__, "dirname is too long.\n");
	sprintf(path, "%s\\%s", module_dir, direntp->d_name);
	stat(path,&st);
 	if (S_ISDIR(st.st_mode)){
		dirs[index] = malloc(strlen(path) + 1);
		sprintf(dirs[index], "%s", path);
		index++;
	}

    }
    int i;
    for(i = 0; i < index; i++){
	    WIN_load_module_fromdir(dirs[i]);
	    free(dirs[i]);
    }
}

int load_common_class(){
	init_common_class();
	return 0;
}

void print_class_list()
{
	char **class_list;
	char **attr_list;
	char **attr_info_list;
	char **iface_list;
	class_list = get_class_list();
	if(class_list == NULL){
		printf("class_list == NULL");
		goto ret;
	}
	int i, j, k;
	for(i = 0; class_list[i] != NULL; i++){
		printf("\n******************************************************\n");
		printf("*%-13s+%5s%-33s*\n", "ClassName", " ",class_list[i]);
		printf("*+ + + + + + + + + + + + + + + + + + + + + + + + + + *\n");
		printf("*%-13s+%38s*\n", "InterfaceName", " ");
		iface_list = get_interface_list(class_list[i]);
		if(iface_list != NULL){
			for(j = 0; iface_list[j] != NULL; j++)
				printf("*%-13s+%5s%-33s*\n", " ", " ", iface_list[j]);
		}
		printf("*+ + + + + + + + + + + + + + + + + + + + + + + + + + *\n");
		attr_list = get_attr_list(class_list[i]);
		if(attr_list != NULL){
			for(j = 0; attr_list[j] != NULL; j++){
				printf("*%-13s+%5s%-33s*\n", "AttributeName", " ",attr_list[j]);
				attr_info_list = get_attr_info(class_list[i], attr_list[j]);
				if(attr_info_list != NULL){
						printf("*%13s+%5s%-33s*\n", "Type", " ",attr_info_list[1]);
						printf("*%13s+%5s%-33s*\n", "Des", " ", attr_info_list[2]);
				}
				printf("*+ + + + + + + + + + + + + + + + + + + + + + + + + + *\n");
			}
		}
	} 
ret:
	return ;
}

int WIN_memory_read_char(char *cpuname, uint32_t addr){
	conf_object_t *core = get_conf_obj(cpuname);
	uint32_t val = 0xff;
	if(core == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get obj for %s.\n", cpuname);
		return val;
	}
	memory_space_intf* iface = (memory_space_intf*)SKY_get_iface(core, MEMORY_SPACE_INTF_NAME);
	if(iface != NULL){
		iface->read(core, addr, &val, 1);
	}
	return (int)(val & 0xff);
}

int WIN_get_cpu_address_width(char *cpuname){
	conf_object_t *core = get_conf_obj(cpuname);
	if(core == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get obj for %s.\n", cpuname);
		return 0;
	}
	core_info_intf* core_info = (core_info_intf*)SKY_get_iface(core, CORE_INFO_INTF_NAME);
	if(core_info == NULL){
		return 0;
	}
	return core_info->get_address_width(core);
}


int WIN_write_device4(char *device, uint32_t offset, uint32_t data)
{
	conf_object_t *obj = get_conf_obj(device);
	if(device == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get obj for %s.\n", device);
		return Error;
	}
	memory_space_intf* io_memory = (memory_space_intf*)SKY_get_iface(obj, MEMORY_SPACE_INTF_NAME);
	io_memory->write(obj, offset, &data, 4);
	return Ok;
}

uint32_t WIN_read_device4(char *device, uint32_t offset)
{
	uint32_t data;
	conf_object_t *obj = get_conf_obj(device);
	if(device == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get obj for %s.\n", device);
		return Error;
	}
	memory_space_intf* io_memory = (memory_space_intf*)SKY_get_iface(obj, MEMORY_SPACE_INTF_NAME);
	io_memory->read(obj, offset, &data, 4);
	return data;
}

int WIN_write_device(char *device, uint32_t offset, char *buf, uint32_t size)
{
	int i;
	conf_object_t *obj = get_conf_obj(device);
	if(device == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get obj for %s.\n", device);
		return Error;
	}
	memory_space_intf* io_memory = (memory_space_intf*)SKY_get_iface(obj, MEMORY_SPACE_INTF_NAME);
	for(i = 0; i < size; i++)
		buf[i] = (buf[i]) & 0xff;
	io_memory->write(obj, offset, buf, size);
	return Ok;
}

uint32_t WIN_set_fj(char *mp, uint32_t addr, uint32_t bit, uint32_t mode){
	exception_t ret;
	uint32_t buf;
	conf_object_t *obj = get_conf_obj(mp);
	if(obj == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get obj for %s.\n", obj);
		return Error;
	}
	memory_space_plug_t* dev = obj->obj;
	memory_space_set_plug_intf* plug_iface = (memory_space_set_plug_intf*)SKY_get_iface(obj, MEMORY_SPACE_SET_PLUG_INTF_NAME);
	if(plug_iface == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get %s interface for %s.\n", CORE_INFO_INTF_NAME);
		return Error;
	}
	ret = plug_iface->set_plug(obj, addr, bit, mode);

	memory_space_intf *iface = (memory_space_intf*)SKY_get_iface(obj, MEMORY_SPACE_INTF_NAME);
	if(iface == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get %s interface for %s.\n", CORE_INFO_INTF_NAME);
		return Error;
	}
	ret = iface->read(obj, addr, &buf, 4);

	if (mode == 0){
		buf &= ~(0x1 << bit);	//set bit : 0		
	}else if (mode == 1){
		buf |= (0x1 << bit);	//set bit : 1		
	}else if (mode == 2){    //reverse bit :if 0: set 1; if 1: set 0
		if (buf & (0x1 << bit)){
			buf &= ~(0x1 << bit); 
		}else {
			buf |= (0x1 << bit);
		}
	}else {
		skyeye_log(Error_log, __FUNCTION__, "Can not set mode %d.\n", mode);
	}
	ret = iface->write(obj, addr, &buf, 4);
	dev->cnt ++;
	dev->buf[dev->cnt] = buf;
	return  Ok;
}

void * WIN_get_fj(char *mp, uint32_t addr, uint32_t bit, uint32_t mode){
	exception_t ret;
	conf_object_t *obj = get_conf_obj(mp);
	if(obj == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get obj for %s.\n", obj);
		return NULL;
	}

	memory_space_set_plug_intf*iface = (memory_space_set_plug_intf*)SKY_get_iface(obj, MEMORY_SPACE_SET_PLUG_INTF_NAME);
	if(iface == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get %s interface for %s.\n", CORE_INFO_INTF_NAME);
		return NULL;
	}
	dev_fi_t *fj_l = iface->get_plug(obj, addr);
	int i;
	if(fj_l == NULL)
		return NULL;
	return fj_l;


}

uint32_t WIN_clear_fj(char *mp, uint32_t addr, uint32_t bit, uint32_t mode){
	exception_t ret;
	conf_object_t *obj = get_conf_obj(mp);
	uint32_t buf;
	if(obj == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get obj for %s.\n", obj);
		return Error;
	}
	memory_space_plug_t* dev = obj->obj;

	memory_space_set_plug_intf *plug_iface = (memory_space_set_plug_intf*)SKY_get_iface(obj, MEMORY_SPACE_SET_PLUG_INTF_NAME);
	if(plug_iface == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get %s interface for %s.\n", CORE_INFO_INTF_NAME);
		return Error;
	}
	ret = plug_iface->clear_plug(obj, addr, bit, mode);
	if (ret != No_exp)
		return Error;

	buf = dev->buf[dev->cnt];
	if (mode == 0){
		buf -= (0<<bit);
	}else if (mode == 1){
		buf -= (1<<bit);
		if ((buf >> 31) & 0x1)
			buf = 0;
	}else if (mode == 2){    //reverse bit :if 0: set 1; if 1: set 0
		if (buf & (0x1 << bit)){
			buf -= (1<<bit);
			if ((buf >> 31) & 0x1)
				buf = 0;
		}else {
			buf -= (0<<bit);
		}
	}else {
		skyeye_log(Error_log, __FUNCTION__, "Can not set mode %d.\n", mode);
	}

	memory_space_intf *iface = (memory_space_intf*)SKY_get_iface(obj, MEMORY_SPACE_INTF_NAME);
	if(iface == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get %s interface for %s.\n", CORE_INFO_INTF_NAME);
		return Error;
	}

	ret = iface->write(obj, addr, &buf, 4);
	dev->buf[dev->cnt] = buf;
	return  Ok;
}

int WIN_connect(char *con_objname, char *iface_objname, char *name, uint32_t index){
	conf_object_t *con_obj = get_conf_obj(con_objname);
	if(con_obj == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get obj for %s.\n", con_objname);
		return Error;
	}
	conf_object_t *iface_obj = get_conf_obj(iface_objname);
	if(iface_obj == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get obj for %s.\n", iface_objname);
		return Error;
	}
	exception_t ret = skyeye_connect(con_obj, iface_obj, name, index);
	if (ret != No_exp){
		skyeye_log(Error_log, __FUNCTION__, "Connect %s and %s fail %s.\n", con_objname, iface_objname);
		return Error;
	}
	return Ok;
}

int WIN_memory_space_add_map(char *memory_space_name, char *device_name, uint32_t address, uint32_t length){
	conf_object_t *memory_space = get_conf_obj(memory_space_name);
	if(memory_space == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get obj for %s.\n", memory_space_name);
		return Error;
	}
	conf_object_t *device = get_conf_obj(device_name);
	if(device == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get obj for %s.\n", device_name);
		return Error;
	}
	memory_space_add_map_intf* iface = (memory_space_add_map_intf*)SKY_get_iface(memory_space, MEMORY_SPACE_ADD_MAP_NAME);
	exception_t ret =  iface->add_map(memory_space, device, address, length);
	if(ret != No_exp)
		return Error;

	return Ok;
}


uint32_t x86_get_device_num(char *cpu)
{
	conf_object_t *obj = get_conf_obj(cpu);
	if(obj == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get obj for %s.\n", cpu);
		return Error;
	}

	core_devices_list *iface = SKY_get_iface(obj, DEVICE_LIST_INTF_NAME);
	if(iface == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get %s interface from %s.\n", CORE_INFO_INTF_NAME, cpu);
		return Error;
	}
	return iface->get_devs_num(obj);
}


char *x86_get_device_name(char *cpu, int did)
{
	conf_object_t *obj = get_conf_obj(cpu);
	if(obj == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get obj for %s.\n", cpu);
		return Error;
	}

	core_devices_list *iface = SKY_get_iface(obj, DEVICE_LIST_INTF_NAME);
	if(iface == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get %s interface from %s.\n", CORE_INFO_INTF_NAME, cpu);
		return Error;
	}
	return iface->get_devs_name(obj, did);


}


uint32_t x86_get_device_reg_num(char *cpu, char *did)
{
	conf_object_t *obj = get_conf_obj(cpu);
	if(obj == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get obj for %s.\n", cpu);
		return Error;
	}

	core_devices_list *iface = SKY_get_iface(obj, DEVICE_LIST_INTF_NAME);
	if(iface == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get %s interface from %s.\n", CORE_INFO_INTF_NAME, cpu);
		return Error;
	}
	iface->get_devs_reg_num(obj, did);

}

char *x86_get_device_reg_name(char *cpu, int did, int rid)
{
	conf_object_t *obj = get_conf_obj(cpu);
	if(obj == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get obj for %s.\n", cpu);
		return Error;
	}

	core_devices_list *iface = SKY_get_iface(obj, DEVICE_LIST_INTF_NAME);
	if(iface == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get %s interface from %s.\n", CORE_INFO_INTF_NAME, cpu);
		return Error;
	}
	return iface->get_devs_reg_name(obj, did, rid);
}

uint32_t x86_get_device_reg_value(char *cpu, int did, int rid)
{
	conf_object_t *obj = get_conf_obj(cpu);
	if(obj == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get obj for %s.\n", cpu);
		return Error;
	}

	core_devices_list *iface = SKY_get_iface(obj, DEVICE_LIST_INTF_NAME);
	if(iface == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get %s interface from %s.\n", CORE_INFO_INTF_NAME, cpu);
		return Error;
	}
	return iface->get_devs_reg_val(obj, did, rid);
}

uint32_t x86_get_device_reg_large_value(char *cpu, int did, int rid, int offset)
{
	conf_object_t *obj = get_conf_obj(cpu);
	if(obj == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get obj for %s.\n", cpu);
		return Error;
	}

	core_devices_list *iface = SKY_get_iface(obj, DEVICE_LIST_INTF_NAME);
	if(iface == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get %s interface from %s.\n", CORE_INFO_INTF_NAME, cpu);
		return Error;
	}
	return iface->get_devs_reg_large_val(obj, did, rid, offset);
}

uint32_t x86_get_device_reg_width(char *cpu, int did)
{
	conf_object_t *obj = get_conf_obj(cpu);
	if(obj == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get obj for %s.\n", cpu);
		return Error;
	}

	core_devices_list *iface = SKY_get_iface(obj, DEVICE_LIST_INTF_NAME);
	if(iface == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get %s interface from %s.\n", CORE_INFO_INTF_NAME, cpu);
		return Error;
	}
	return iface->get_devs_reg_width(obj, did);
}

uint32_t x86_get_device_reg_addr(char *cpu, int did, int rid)
{
	conf_object_t *obj = get_conf_obj(cpu);
	if(obj == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get obj for %s.\n", cpu);
		return Error;
	}

	core_devices_list *iface = SKY_get_iface(obj, DEVICE_LIST_INTF_NAME);
	if(iface == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get %s interface from %s.\n", DEVICE_FIN_INTF_NAME, cpu);
		return Error;
	}
	return (void *)(iface->get_devs_reg_addr(obj, did, rid));
}

void x86_device_save_fin(char *cpu, int did, uint32_t addr, int bitnum, int mode)
{
	conf_object_t *obj = get_conf_obj(cpu);
	if(obj == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get obj for %s.\n", cpu);
		return ;
	}

	core_devices_fin *iface = SKY_get_iface(obj,DEVICE_FIN_INTF_NAME);
	if(iface == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get %s interface from %s.\n", DEVICE_FIN_INTF_NAME, cpu);
		return;
	}
	return iface->devs_save_fin(obj, did, addr, bitnum, mode);
}


void *x86_device_get_fin(char *cpu, int did)
{
	conf_object_t *obj = get_conf_obj(cpu);
	if(obj == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get obj for %s.\n", cpu);
		return NULL;
	}

	core_devices_fin *iface = SKY_get_iface(obj, DEVICE_FIN_INTF_NAME);
	if(iface == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get %s interface from %s.\n", DEVICE_FIN_INTF_NAME, cpu);
		return NULL;
	}
	return (void *)(iface->devs_get_fin(obj, did));
}

void x86_device_clear_fin(char *cpu, int did, uint32_t addr, int bitnum, int mode)
{
	conf_object_t *obj = get_conf_obj(cpu);
	if(obj == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get obj for %s.\n", cpu);
		return ;
	}

	core_devices_fin *iface = SKY_get_iface(obj, DEVICE_FIN_INTF_NAME);
	if(iface == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get %s interface from %s.\n", DEVICE_FIN_INTF_NAME, cpu);
		return ;
	}
	return iface->devs_clear_fin(obj, did, addr, bitnum, mode);
}


int x86_save_configure(char *cpuname, char *filename)
{
#if 0
	conf_object_t *core = get_conf_obj(cpuname);
	if(core == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get obj for %s.\n", cpuname);
		return Error;
	}
		
	x86_checkpoint_intf* iface = (x86_checkpoint_intf*)SKY_get_iface(core, X86_CHECKPOINT_INTF_NAME);
	exception_t ret =  iface->save_configure(core, filename);
	if(ret != No_exp)
		return Error;
#endif
	return Ok;
}


int x86_load_configure(char *cpuname, char *filename)
{
#if 0
	conf_object_t *core = get_conf_obj(cpuname);
	if(core == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get obj for %s.\n", cpuname);
		return Error;
	}
		
	x86_checkpoint_intf* iface = (x86_checkpoint_intf*)SKY_get_iface(core, X86_CHECKPOINT_INTF_NAME);
	exception_t ret =  iface->load_configure(core, filename);
	if(ret != No_exp)
		return Error;
#endif
	return Ok;
}
void x86_reset(char *cpu)
{
	conf_object_t *obj = get_conf_obj(cpu);
	if(obj == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get obj for %s.\n", cpu);
		return ;
	}

	x86_core_intf *iface = SKY_get_iface(obj, X86_CORE_INTF_NAME);
	if(iface == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get %s interface from %s.\n", X86_CORE_INTF_NAME, cpu);
		return ;
	}
	return iface->x86_reset(obj);
}

int WIN_store_configure(char *filename)
{
	//store_to_configure(filename);
	return Ok;
}

int WIN_recover_configure(char *filename)
{
	//recover_from_configure(filename);
	return Ok;
}


int32_t WIN_image_get_page_size(char *imagename){
	conf_object_t *image = get_conf_obj(imagename);
	uint32_t page_size;
	if(image == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get obj for %s.\n", imagename);
		return Error;
	}
	skyeye_image_info *iface = SKY_get_iface(image, SKYEYE_IMAGE_INFO);
	if(iface == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get %s interface from %s.\n", DEVICE_FIN_INTF_NAME, imagename);
		return Error;
	}
	page_size = iface->get_page_size(image);
	return page_size;
}

int32_t WIN_image_get_page_count(char *imagename){
	conf_object_t *image = get_conf_obj(imagename);
	uint32_t page_count;
	if(image == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get obj for %s.\n", imagename);
		return Error;
	}
	skyeye_image_info *iface = SKY_get_iface(image, SKYEYE_IMAGE_INFO);
	if(iface == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get %s interface from %s.\n", DEVICE_FIN_INTF_NAME, imagename);
		return Error;
	}
	page_count = iface->get_page_count(image);
	return page_count;
}

int32_t WIN_image_get_page_index_by_id(char *imagename, uint32_t id){
	conf_object_t *image = get_conf_obj(imagename);
	if(image == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get obj for %s.\n", imagename);
		return Error;
	}
	skyeye_image_info *iface = SKY_get_iface(image, SKYEYE_IMAGE_INFO);
	if(iface == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get %s interface from %s.\n", DEVICE_FIN_INTF_NAME, imagename);
		return Error;
	}
	return iface->get_page_index_by_id(image, id);
}

int32_t WIN_image_page_to_file(char *imagename, uint32_t index, char *filename){
	conf_object_t *image = get_conf_obj(imagename);
	uint32_t page_size, count;
	if(image == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get obj for %s.\n", imagename);
		return Error;
	}
	skyeye_image_info *iface = SKY_get_iface(image, SKYEYE_IMAGE_INFO);
	if(iface == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get %s interface from %s.\n", DEVICE_FIN_INTF_NAME, imagename);
		return Error;
	}
	page_size = iface->get_page_size(image);
	char *page_base = iface->get_page_content(image, index);
	if(page_base == NULL){
		return Error;
	}
	FILE *fp = fopen(filename, "wb+");
	if(fp == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Create %s file fail\n", filename);
	}
	count = fwrite(page_base, 1, page_size, fp);
	fclose(fp);
	if(count != page_size){
		skyeye_log(Error_log, __FUNCTION__, "Write %d bytes to file %s, less than the correct amount.\n",count, filename);
		return Error;
	}
	return Ok;
}


int32_t WIN_image_file_to_page(char *imagename, uint32_t index, char *filename){
	conf_object_t *image = get_conf_obj(imagename);
	uint32_t page_size, count;
	char *page_ptr = NULL;
	if(image == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get obj for %s.\n", imagename);
		return Error;
	}
	skyeye_image_info *iface = SKY_get_iface(image, SKYEYE_IMAGE_INFO);
	if(iface == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get %s interface from %s.\n", DEVICE_FIN_INTF_NAME, imagename);
		return Error;
	}
	page_size = iface->get_page_size(image);
	page_ptr = skyeye_mm_zero(page_size + 1);
	FILE *fp = fopen(filename, "rb");
	if(fp == NULL){
		skyeye_log(Error_log, __FUNCTION__, "open %s file fail\n", filename);
	}
	count = fread(page_ptr, 1, page_size, fp);
	if(count != page_size){
		skyeye_log(Error_log, __FUNCTION__, "Read %d bytes from file %s, less than the correct amount.\n",count, filename);
		skyeye_free(page_ptr);
		return Error;
	}
	fclose(fp);
	iface->set_page_content(image, page_ptr, index);
	skyeye_free(page_ptr);
	return Ok;
}

int32_t WIN_image_clear_all_pages(char *imagename){
	conf_object_t *image = get_conf_obj(imagename);
	uint32_t page_size, count;
	char *page_ptr = NULL;
	if(image == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get obj for %s.\n", imagename);
		return Error;
	}
	skyeye_image_info *iface = SKY_get_iface(image, SKYEYE_IMAGE_INFO);
	if(iface == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get %s interface from %s.\n", DEVICE_FIN_INTF_NAME, imagename);
		return Error;
	}
	iface->clear_all_pages(image);
	return Ok;
}

uint64_t WIN_get_cpu_steps(char *cpuname)
{
	uint64_t steps;
	conf_object_t *core = get_conf_obj(cpuname);
	if(core == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get obj for %s.\n", cpuname);
		return Error;
	}
	core_info_intf* core_info = (core_info_intf*)SKY_get_iface(core, CORE_INFO_INTF_NAME);
	steps = core_info->get_steps(core);
	return steps;
}

int  WIN_set_cpu_mode(char *cpuname, int mode)
{
	conf_object_t *core = get_conf_obj(cpuname);
	if(core == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get obj for %s.\n", cpuname);
		return Error;
	}
	attr_value_t value;
	if(!SKY_has_attribute(core, "mode")){
		value = SKY_make_attr_uinteger(mode);
		SKY_set_attribute(core, "mode", &value);
	}
	return Ok;
}


int WIN_get_executed_pc_file(char *instr_process_name, char *fname){
	conf_object_t *instr_process = get_conf_obj(instr_process_name);
	if(instr_process == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get obj for %s.\n", instr_process_name);
		return Error;
	}
	instr_process_intf* iface= (instr_process_intf*)SKY_get_iface(instr_process, INSTR_PROCESS_INTF);
	iface->get_dif_pc_cache(instr_process, fname);
	return Ok;
}
int WIN_get_pc_record_size(char *instr_process_name) {
	conf_object_t *instr_process = get_conf_obj(instr_process_name);
	if(instr_process == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get obj for %s.\n", instr_process_name);
		return Error;
	}
	instr_process_intf* iface= (instr_process_intf*)SKY_get_iface(instr_process, INSTR_PROCESS_INTF);
	return iface->get_pc_record_size(instr_process);
}
int WIN_set_pc_record_size(char *instr_process_name, int size) {
	conf_object_t *instr_process = get_conf_obj(instr_process_name);
	if(instr_process == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get obj for %s.\n", instr_process_name);
		return Error;
	}
	instr_process_intf* iface= (instr_process_intf*)SKY_get_iface(instr_process, INSTR_PROCESS_INTF);
	return iface->set_pc_record_size(instr_process, size);
}
int WIN_get_pc_record_nums(char *instr_process_name) {
	conf_object_t *instr_process = get_conf_obj(instr_process_name);
	if(instr_process == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get obj for %s.\n", instr_process_name);
		return Error;
	}
	instr_process_intf* iface= (instr_process_intf*)SKY_get_iface(instr_process, INSTR_PROCESS_INTF);
	return iface->get_pc_record_nums(instr_process);
}
int WIN_get_pc_record_index(char *instr_process_name) {
	conf_object_t *instr_process = get_conf_obj(instr_process_name);
	if(instr_process == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get obj for %s.\n", instr_process_name);
		return Error;
	}
	instr_process_intf* iface= (instr_process_intf*)SKY_get_iface(instr_process, INSTR_PROCESS_INTF);
	return iface->get_pc_record_index(instr_process);
}
int WIN_get_pc_record_overflow(char *instr_process_name) {
	conf_object_t *instr_process = get_conf_obj(instr_process_name);
	if(instr_process == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get obj for %s.\n", instr_process_name);
		return Error;
	}
	instr_process_intf* iface= (instr_process_intf*)SKY_get_iface(instr_process, INSTR_PROCESS_INTF);
	return iface->get_pc_record_overflow(instr_process);
}
int WIN_get_pc_by_index(char *instr_process_name, int n) {
	conf_object_t *instr_process = get_conf_obj(instr_process_name);
	if(instr_process == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get obj for %s.\n", instr_process_name);
		return Error;
	}
	instr_process_intf* iface= (instr_process_intf*)SKY_get_iface(instr_process, INSTR_PROCESS_INTF);
	return iface->get_pc_by_index(instr_process, n);
}

char * WIN_get_architecture(char *cpuname)
{
	conf_object_t *core = get_conf_obj(cpuname);
	if(core == NULL){
		skyeye_log(Error_log, __FUNCTION__, "Can not get obj for %s.\n", cpuname);
		return NULL;
	}

	core_info_intf* iface = (core_info_intf*)SKY_get_iface(core, CORE_INFO_INTF_NAME);
	return iface->get_architecture(core);
}

void mm_info_cmd(char *args) {
#ifdef IS_MM_TEST
    if (!strcmp(args, "log")) {
        set_mm_save_log(1);
        printf("Set log save Ok!\n");
    } else if (!strcmp(args, "nolog")) {
        set_mm_save_log(0);
        printf("Cancel save Ok!\n");
    } else {
        mm_info_print(args);
    }
#else
    printf("mm_info command failure\n");
#endif
}
int com_save_image(char *devname){
	conf_object_t *obj;
	skyeye_image_info *image_info;
	memory_space_intf *mm_space_intf;
	attr_value_t attr;
	uint32_t size, page_size, cnt, ret;
	time_t tm;
	struct tm *ptm;
	/* suffix: 20160714200856, filename: ${devname}.${suffix} */
	char suffix[15], filename[128], fullname[512];
	FILE *pfile;
	char *one_page;
	uint32_t page_count, i;
	attr_value_t save_path_attr;

	obj		= (conf_object_t *)get_conf_obj(devname);

	image_info	= (skyeye_image_info *)SKY_get_iface(obj, SKYEYE_IMAGE_INFO);
	mm_space_intf	= (memory_space_intf *)SKY_get_iface(obj, MEMORY_SPACE_INTF_NAME);
	attr		= SKY_get_attribute(obj, "size");
	save_path_attr	= SKY_get_attribute(obj, "save_path");

	if (image_info == NULL || mm_space_intf == NULL || attr.type == Val_Invalid) {
		printf("[%s:%s:%d]: image_info or mm_space_intf or get_attribute faild.\n",
				__FILE__, __FUNCTION__, __LINE__);
		return -1;
	}

	tm		= time(NULL);
	ptm		= localtime(&tm);

	strftime(suffix, sizeof(suffix), "%Y%m%d%H%M%S", ptm);
	snprintf(filename, sizeof(filename), "%s.%s", devname, suffix);
	snprintf(fullname, sizeof(fullname), "%s%s", SKY_attr_string(save_path_attr), filename);

	if ((pfile = fopen(fullname, "wb")) == NULL) {
		printf("[%s:%s:%d]: fopen error.\n", __FILE__, __FUNCTION__, __LINE__);
		return -1;
	}

	size		= SKY_attr_uinteger(attr);
	page_count	= image_info->get_page_count(obj);
	page_size	= image_info->get_page_size(obj);

	printf("DEVNAME: %s\tIMAGE BANK_SIZE(PAGE_SIZE): 0x%x, PAGE_COUNT: %u\tACTUAL_TOTAL_SIZE: 0x%x\n",
			devname, page_size, page_count, size);

	for (i = 0; i < page_count; i++) {
		one_page = image_info->get_page_content(obj, i);

		cnt = 0;
		do {
			ret = fwrite(&one_page[cnt], 1, page_size - cnt, pfile);
			if (ret == 0) {
				printf("[%s:%s:%d]: fwrite error.\n", __FILE__, __FUNCTION__, __LINE__);
				return -1;
			}
			cnt += ret;
		} while (cnt < page_size);
	}

	fclose(pfile);

	printf("Save done!\n");

	return 0;
}
