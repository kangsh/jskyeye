/**
 * @file translate.cpp
 * 
 * This translates a single instruction by calling out into
 * the architecture dependent functions. It will optionally
 * create internal basic blocks if necessary.
 *
 * @author OS Center,TsingHua University (Ported from libcpu)
 * @date 11/11/2010
 */

#include "llvm/Instructions.h"
#include "llvm/BasicBlock.h"

#include "skyeye_dyncom.h"
#include "dyncom/tag.h"
#include "dyncom/basicblock.h"
#include "dyncom/frontend.h"
#include "dyncom/dyncom_llvm.h"
#include "dyncom/defines.h"
#include "llvm/Constants.h"
#include "bank_defs.h"
#include "portable/portable.h"

static void check_intr(cpu_t *cpu,BasicBlock *bb_cur,BasicBlock *bb_trap,BasicBlock *bb_target){
	LoadInst *v_cpsr  =  new LoadInst(cpu->ptr_gpr[16],"",false,bb_cur);
	LoadInst *v_Nirq_sig  =  new LoadInst(cpu->ptr_Nirq,"",false,bb_cur);
	Value *int_en = BinaryOperator::Create(Instruction::And,v_cpsr,CONST(0x80),"",bb_cur);
	Value *irq_pending = BinaryOperator::Create(Instruction::Or,int_en,v_Nirq_sig,"",bb_cur);
	Value *gout = new ICmpInst(*bb_cur,ICmpInst::ICMP_EQ,irq_pending,CONST(0),"");
	BranchInst::Create(bb_trap,bb_target,gout,bb_cur);
}

static inline BasicBlock*
attach_check_event_if_cycle(cpu_t *cpu, addr_t pc, BasicBlock *bb_target, BasicBlock *bb_trap) {
	br_target_set_it it;
	it = cpu->br_target_set.find(pc);
	if (it == cpu->br_target_set.end()) {
		return bb_target;
	} else {
		BasicBlock* bb = BasicBlock::Create(_CTX(), "CE_CYCLE_timeout", cpu->dyncom_engine->cur_func, 0);
		BasicBlock* bb_io = BasicBlock::Create(_CTX(), "CE_CYCLE_io", cpu->dyncom_engine->cur_func, 0);

		// test timeout
		Value* icounter = arch_get_reg_by_ptr(cpu, &cpu->icounter, 64, bb);
		Value* timeout_icounter = arch_get_reg_by_ptr(cpu, &cpu->timeout_icounter, 64, bb);
		Value *gout = new ICmpInst(*bb, ICmpInst::ICMP_UGE, icounter, timeout_icounter, "");
		BranchInst::Create(cpu->dyncom_engine->bb_timeout, bb_io, gout, bb);

		events_handler(cpu, pc, bb_io, bb_target, bb_trap);
		return bb;
	}
}

static const BasicBlock *
lookup_basicblock_or_fail(cpu_t *cpu, Function* f, addr_t pc,
	BasicBlock *bb_ret,
	BasicBlock *bb_trap,
	uint8_t bb_type) {
	// lookup for the basicblock associated to pc in specified function 'f'
	bbaddr_map &bb_addr = cpu->dyncom_engine->func_bb[f];
	bbaddr_map::const_iterator i = bb_addr.find(pc);
	if (i != bb_addr.end()) {
		cpu->br_target_set.insert(pc);
		return attach_check_event_if_cycle(cpu, pc, i->second, bb_trap);
	} else
		return bb_ret;
}

static void
get_target_block(cpu_t *cpu, addr_t pc, int phase, /* 0 = branch, 1 = delay slot */
	BasicBlock *bb_target,
	BasicBlock *bb_ret,
	BasicBlock *bb_trap,
	BasicBlock *cur_bb)
{
	BasicBlock *bb = NULL, *bb_next = NULL;
	sparc_br_ctx_t *ctx = &cpu->sparc_br_ctx;

	switch (ctx->type) {
	case UC_T_BRANCH:
	case UC_NT_BRANCH:
		if (phase == 0) {
			if (ctx->annulled) {
				bb = (BasicBlock*)lookup_basicblock_or_fail(cpu, cpu->dyncom_engine->cur_func,
					ctx->target_addr, bb_ret, bb_trap, BB_TYPE_NORMAL);
			} else {
				bb = (BasicBlock*)lookup_basicblock_or_fail(cpu, cpu->dyncom_engine->cur_func,
					pc + 4, bb_ret, bb_trap, BB_TYPE_NORMAL);
			}
			BranchInst::Create(bb, cur_bb);
		} else {
			if (!ctx->annulled) { // if annulled, this slot should continued to next if it's a entry
				bb = (BasicBlock*)lookup_basicblock_or_fail(cpu, cpu->dyncom_engine->cur_func,
					ctx->target_addr, bb_ret, bb_trap, BB_TYPE_NORMAL);
			} else {
				bb = (BasicBlock*)lookup_basicblock_or_fail(cpu, cpu->dyncom_engine->cur_func,
					pc + 4, bb_ret, bb_trap, BB_TYPE_NORMAL);
			}
			BranchInst::Create(bb, cur_bb);
			CLEAR_BR_CTX;
		}
		break;
	case COND_BRANCH:
		if (phase == 0) {
			if (ctx->annulled) {
				bb = (BasicBlock*)lookup_basicblock_or_fail(cpu, cpu->dyncom_engine->cur_func,
					pc + 4 + 4, bb_ret, bb_trap, BB_TYPE_NORMAL);
				bb_next = (BasicBlock*)lookup_basicblock_or_fail(cpu, cpu->dyncom_engine->cur_func,
					pc + 4, bb_ret, bb_trap, BB_TYPE_NORMAL);
				BranchInst::Create(bb_next, bb, ctx->cond, cur_bb);
			} else {
				bb = (BasicBlock*)lookup_basicblock_or_fail(cpu, cpu->dyncom_engine->cur_func,
					pc + 4, bb_ret, bb_trap, BB_TYPE_NORMAL);
				BranchInst::Create(bb, cur_bb);
			}
			ctx->cond = NULL;
		} else {
			bb = (BasicBlock*)lookup_basicblock_or_fail(cpu, cpu->dyncom_engine->cur_func,
				ctx->target_addr, bb_ret, bb_trap, BB_TYPE_NORMAL);
			bb_next = (BasicBlock*)lookup_basicblock_or_fail(cpu, cpu->dyncom_engine->cur_func,
				pc + 4, bb_ret, bb_trap, BB_TYPE_NORMAL);
			BranchInst::Create(bb, bb_next, ctx->cond, cur_bb);

			CLEAR_BR_CTX;
		}
		break;
	case CALL_BRANCH:
		if (phase == 0) {
			bb = (BasicBlock*)lookup_basicblock_or_fail(cpu, cpu->dyncom_engine->cur_func,
				pc + 4, bb_ret, bb_trap, BB_TYPE_NORMAL);
			BranchInst::Create(bb, cur_bb);
		} else {
			bb = (BasicBlock*)lookup_basicblock_or_fail(cpu, cpu->dyncom_engine->cur_func,
				ctx->target_addr, bb_ret, bb_trap, BB_TYPE_NORMAL);
			BranchInst::Create(bb, cur_bb);
			CLEAR_BR_CTX;
		}
		break;
	case NOT_BRANCH:
		BranchInst::Create(bb_target, cur_bb);
		break;
	default:
		break;
	}
}

/**
 * @brief translate a single instruction in general.
 *
 * @param cpu CPU core structure
 * @param pc current address to translate
 * @param tag tag of the address
 * @param bb_target target basic block for branch/call instruction
 * @param bb_trap target basic block for trap
 * @param bb_next non-taken for conditional,the next instruction's basic block
 * @param bb_ret return basic block
 * @param cur_bb current basic block
 *
 * @return returns the basic block where code execution continues, or
 *	NULL if the instruction always branches away
 *	(The caller needs this to link the basic block)
 */
static
BasicBlock *
translate_instr_general(cpu_t *cpu, addr_t pc, addr_t next_pc, tag_t tag,
	BasicBlock *bb_target,	/* target for branch/call/rey */
	BasicBlock *bb_trap,	/* target for trap */
	BasicBlock *bb_next,	/* non-taken for conditional */
	BasicBlock *bb_ret, BasicBlock *cur_bb)
{
	BasicBlock *bb_cond = NULL;
	/* Get the current instruction length */
	uint32 instr_length = cpu->f.get_instr_length(cpu);
	/* create internal basic blocks if needed */
	if (tag & TAG_CONDITIONAL)
		bb_cond = create_basicblock(cpu, pc, cpu->dyncom_engine->cur_func, BB_TYPE_COND);

	if (tag & TAG_CONDITIONAL) {
		// cur_bb:  if (cond) goto b_cond; else goto bb_next;
		Value *c = cpu->f.translate_cond(cpu, pc, cur_bb);
		if((tag & TAG_END_PAGE) && !is_user_mode(cpu)){
			emit_store_pc_cond(cpu, tag, c, cur_bb, next_pc);
			BranchInst::Create(bb_cond, bb_ret, c, cur_bb);
		}
		else{
			#if 0
			if (tag & TAG_BEFORE_SYSCALL){
				emit_store_pc(cpu, cur_bb, next_pc);
				BranchInst::Create(bb_cond, bb_trap, c, cur_bb);
			}
			else
			#endif
			BranchInst::Create(bb_cond, bb_next, c, cur_bb);
		}
		cur_bb = bb_cond;
	}
	if ((tag & TAG_NEW_BB) && !is_user_mode(cpu)) { //&& !(tag & TAG_BRANCH)) {
		cpu->dyncom_engine->bb_trap = bb_trap;
	}

	cpu->f.translate_instr(cpu, pc, cur_bb);
	if ((tag & TAG_NEW_BB) && !is_user_mode(cpu)) { //&& !(tag & TAG_BRANCH)) {
		cur_bb = cpu->dyncom_engine->bb;
	}
	if ((tag & TAG_NEED_PC) && !is_user_mode(cpu)) {
		BasicBlock *bb = cur_bb;
		if(cpu->info.pc_index_in_gpr != -1){
			Value *vpc = new LoadInst(cpu->ptr_gpr[15], "", false, bb);
			new StoreInst(ADD(vpc, CONST(instr_length)), cpu->ptr_gpr[15], bb);
		}else{
			Value *vpc = arch_get_spr_reg(cpu, cpu->info.phys_pc_index_in_spr, 0, bb);
			arch_put_spr_reg(cpu, cpu->info.pc_index_in_spr, ADD(vpc, CONST(instr_length)), 0, false, bb);
		}
	}
	if (tag & TAG_POSTCOND) {
		Value *c = cpu->f.translate_cond(cpu, pc, cur_bb);
		BranchInst::Create(bb_target, bb_next, c, cur_bb);
	}
	if ((tag & (TAG_END_PAGE | TAG_EXCEPTION)) && !is_user_mode(cpu))
		BranchInst::Create(bb_ret, cur_bb);
	else if (tag & TAG_TRAP) {
		BranchInst::Create(bb_trap, cur_bb);
		/* TAG_TRAP has higher priority than TAG_RETARGET/TAG_RETARGET_COND,
		 * so we should clear_br_ctx here.
		 */
		if (cpu->sparc_br_ctx.type != NOT_BRANCH) {
			CLEAR_BR_CTX;
		}
	} else if (tag & TAG_BRANCH){
		tag_t new_tag;
		addr_t new_pc, dummy2;
		cpu->f.tag_instr(cpu, pc, &new_tag, &new_pc, &dummy2);
		new_tag = get_tag(cpu, new_pc);
		if (new_tag & TAG_TRANSLATED)
			check_intr(cpu,cur_bb,bb_trap,bb_target);
		else
			BranchInst::Create(bb_target, cur_bb);
//		check_intr(cpu,cur_bb,bb_trap,bb_target);
	}
	else if (tag & (TAG_CALL | TAG_RET))
		BranchInst::Create(bb_target,cur_bb);
	else if (tag & TAG_CONDITIONAL) {/* Add terminator instruction 'br' for conditional instruction */
		BranchInst::Create(bb_next, cur_bb);
	}
	else if (tag & (TAG_RETARGET | TAG_RETARGET_COND))
		//get_target_block(cpu, pc, 1, bb_target, bb_ret, bb_trap, cur_bb);
		BranchInst::Create(bb_target, cur_bb);

#if OPT_LOCAL_REGISTERS
#if 0
	if (tag & TAG_BEFORE_SYSCALL) {//bb_instr needs a terminator inst.
		/* the branch instruction before syscall */
		if((tag & TAG_BRANCH)){ 
			/* Do nothing for Unconditional branch */
			printf("In %s, pc=0x%x\n", __FUNCTION__, pc);
		}
		else{
			emit_store_pc_return(cpu, cur_bb, pc + instr_length, bb_trap);
			cur_bb = NULL;
		}
	}
	if (tag & TAG_SYSCALL) {//bb_instr needs a terminator inst.
		emit_store_pc_return(cpu, cur_bb, pc + instr_length, bb_ret);
		cur_bb = NULL;
	}
#endif
#endif

	if (tag & TAG_CONTINUE)
		return cur_bb;
	else
		return NULL;
}

/**
 * @brief translate a single instruction with delay slot.
 *
 * @param cpu CPU core structure
 * @param pc current address to translate
 * @param tag tag of the address
 * @param bb_target target basic block for branch/call instruction
 * @param bb_trap target basic block for trap
 * @param bb_next non-taken for conditional,the next instruction's basic block
 * @param bb_ret return basic block
 * @param cur_bb current basic block
 *
 * @return returns the basic block where code execution continues, or
 *	NULL if the instruction always branches away
 *	(The caller needs this to link the basic block)
 */
static
BasicBlock *
translate_instr_delay(cpu_t *cpu, addr_t pc, addr_t next_pc, tag_t tag,
	BasicBlock *bb_target,	/* target for branch/call/rey */
	BasicBlock *bb_trap,	/* target for trap */
	BasicBlock *bb_next,	/* non-taken for conditional */
	BasicBlock *bb_ret, BasicBlock *cur_bb)
{
	BasicBlock *bb_cond = NULL;

	/* create internal basic blocks if needed */
	if (tag & TAG_CONDITIONAL)
		bb_cond = create_basicblock(cpu, pc, cpu->dyncom_engine->cur_func, BB_TYPE_COND);

	if(!cpu->info.delay_slots) {
		if(tag & TAG_CONDITIONAL) {
			// cur_bb:  if (cond) goto b_cond; else goto bb_next;
			Value *c = cpu->f.translate_cond(cpu, pc, cur_bb);
			if((tag & TAG_END_PAGE) && !is_user_mode(cpu)){
				emit_store_pc_cond(cpu, tag, c, cur_bb, next_pc);
				BranchInst::Create(bb_cond, bb_ret, c, cur_bb);
			}else{
				BranchInst::Create(bb_cond, bb_target, c, cur_bb);
			}
			cur_bb = bb_cond;
		}
		cpu->f.translate_instr(cpu, pc, cur_bb);
		if ((tag & (TAG_END_PAGE | TAG_EXCEPTION)) && !is_user_mode(cpu))
			//get_target_block(cpu, pc, 0, bb_target, bb_ret, bb_trap, cur_bb);
			BranchInst::Create(bb_ret, cur_bb);
		else if (tag & TAG_TRAP)
			BranchInst::Create(bb_trap, cur_bb);
		else if (tag & TAG_BRANCH)
			//get_target_block(cpu, pc, 0, bb_target, bb_ret, bb_trap, cur_bb);
			BranchInst::Create(bb_target, cur_bb);
	}else{
		BasicBlock *bb_delay = NULL;
		if ( (tag & TAG_DELAY_SLOT) && (tag & TAG_CONDITIONAL) && (!(tag & TAG_COND_DELAY)) )
			bb_delay = create_basicblock(cpu, pc, cpu->dyncom_engine->cur_func, BB_TYPE_DELAY);

		addr_t delay_pc, step;
		tag_t delay_tag;
		addr_t new_pc, dummy1;

		if (tag & TAG_CONDITIONAL) {
			// cur_bb:  if (cond) goto b_cond; else goto bb_delay/bb_next;
			Value *c = cpu->f.translate_cond(cpu, pc, cur_bb);
			if((tag & TAG_END_PAGE) && !is_user_mode(cpu)){
				emit_store_pc_cond(cpu, tag, c, cur_bb, next_pc);
				BranchInst::Create(bb_cond, bb_ret, c, cur_bb);
			}else{
				if( !(tag & TAG_COND_DELAY) )
					BranchInst::Create(bb_cond, bb_delay, c, cur_bb);
				else
					// bb_delay: goto bb_next;
					BranchInst::Create(bb_cond, bb_next, c, cur_bb);
			}

			// bb_cond: instr; delay; goto bb_target/bb_trap;
			step = cpu->f.translate_instr(cpu, pc, bb_cond);
			delay_pc = pc + step;
			/* get delay slot tag */
			cpu->f.tag_instr(cpu, delay_pc, &delay_tag, &new_pc, &dummy1);

			BasicBlock *bb;
			bb = translate_instr_general(cpu, delay_pc, next_pc, delay_tag,
					bb_target, bb_trap, bb_next, bb_ret, bb_cond);
			if(bb)
				BranchInst::Create(bb_target, bb);

			if( !(tag & TAG_COND_DELAY) ) {
				// bb_delay: delay; goto bb_next/bb_trap;
				bb = translate_instr_general(cpu, delay_pc, next_pc, delay_tag,
						bb_target, bb_trap, bb_next, bb_ret, bb_delay);
				if(bb)
					BranchInst::Create(bb_next, bb);
			}
		} else {
			if(tag & TAG_TRAP) {
				BranchInst::Create(bb_trap, cur_bb);
			}else{
				// cur_bb:  instr; delay; goto bb_target/bb_trap;
				step = cpu->f.translate_instr(cpu, pc, cur_bb);
				delay_pc = pc + step;
				/* get delay slot tag */
				cpu->f.tag_instr(cpu, delay_pc, &delay_tag, &new_pc, &dummy1);

				BasicBlock *bb;
				bb = translate_instr_general(cpu, delay_pc, next_pc, delay_tag,
						bb_target, bb_trap, bb_next, bb_ret, cur_bb);
				if(bb)
					BranchInst::Create(bb_target, bb);
			}
		}
	}
	return NULL; /* don't link */
}

/**
 * @brief translate a single instruction.
 *
 * @param cpu CPU core structure
 * @param pc current address to translate
 * @param tag tag of the address
 * @param bb_target target basic block for branch/call instruction
 * @param bb_trap target basic block for trap
 * @param bb_next non-taken for conditional,the next instruction's basic block
 * @param bb_ret return basic block
 * @param cur_bb current basic block
 *
 * @return returns the basic block where code execution continues, or
 *	NULL if the instruction always branches away
 *	(The caller needs this to link the basic block)
 */
BasicBlock *
translate_instr(cpu_t *cpu, addr_t pc, addr_t next_pc, tag_t tag,
	BasicBlock *bb_target,	/* target for branch/call/rey */
	BasicBlock *bb_trap,	/* target for trap */
	BasicBlock *bb_next,	/* non-taken for conditional */
	BasicBlock *bb_ret, BasicBlock *cur_bb)
{
	if (tag & (TAG_DELAY_SLOT | TAG_COND_DELAY)) {
		return translate_instr_delay(cpu, pc, next_pc, tag,
				bb_target, bb_trap, bb_next, bb_ret, cur_bb);
	}else{
		return translate_instr_general(cpu, pc, next_pc, tag,
				bb_target, bb_trap, bb_next, bb_ret, cur_bb);
	}
}
