#include <stdio.h>
#include <sim_control.h>
#include <stdlib.h>
#include <default_command.h>
#include <skyeye_loader.h>
#include <skyeye_attr.h>
#include <skyeye_core.h>
extern void init_cortex_a8_core();
extern void init_stm32_mach();
extern void init_ram();
extern void init_image();
extern void init_leon2_uart();
extern void init_uart_term();
static void register_required_class(){
	init_cortex_a8_core();
	init_stm32_mach();
	init_ram();
	init_image();
	init_leon2_uart();
	init_uart_term();
}
int main() {
	printf("begin init\n");
	SIM_init(); /* load and manage all the module and object */
	register_required_class();
	int ret_status = 1;
	ret_status &= WIN_create_mach("stm32_0", "stm32");
	/* register the cortex core */
	if(0 == ret_status){
		printf("create stm32 failed!\n");
		exit(-1);
	}

	ret_status &= WIN_create_cpu("stm32_0", "cpu0", "cortex_a8_core");	
	if(0 == ret_status){
		printf("create cpu failed!\n");
		exit(-1);
	}

	ret_status &= WIN_create_device("stm32_0", "memory_space_0", "memory_space");
	if(0 == ret_status){
		printf("create memory_space failed!\n");
		exit(-1);
	}

	ret_status &= WIN_create_device("stm32_0", "image0","image");
	if(0 == ret_status){
		printf("create image failed!\n");
		exit(-1);
	}

	ret_status &= WIN_create_device("stm32_0", "ram0", "ram");

	ret_status &= WIN_create_device("stm32_0", "uart_term0", "uart_term");
	ret_status &= WIN_create_device("stm32_0", "uart0", "leon2_uart");

	ret_status &= WIN_set_attr("image0", "size", "uinteger", "0x1fffffff");
	ret_status &= WIN_connect("ram0", "image0", "memory_space", 0);

	ret_status &= WIN_connect("uart0", "uart_term0", "skyeye_uart_intf", 0);
	ret_status &= WIN_connect("cpu0", "memory_space_0", "memory_space", 0);
	
	ret_status &= WIN_memory_space_add_map("memory_space_0", "ram0", 0, 0x1fffffff);
	ret_status &= WIN_memory_space_add_map("memory_space_0", "uart0", 0x80000070, 0x10);
	/* register the cortex core */
	if(0 == ret_status){
		printf("create device failed!\n");
		exit(-1);
	}
	//WIN_load_binary("cpu0", "arm_hello");

	/* The load address is up to the ldscript of the testcase */
	load_file(get_conf_obj("cpu0"), "testcase/stm32/arm_hello.bin", 0x1000000);

	prepare_to_run();
	SIM_set_pc(get_conf_obj("cpu0"), 0x1000000);
	printf("finish init\n");

	SIM_run();
	while(1);
	SIM_quit();
	return 0;
}
