#coding:utf-8
import time
import os
import sys
from skyeye_autotest_command import *

class test():
	def __init__(self):
		testname="setup.skyeye"
		test_branch='new_framework'
		enable=True
		self.ac=autotest_command(testname,test_branch,enable)

	def dev_init(self):
		self.ac.add_file_out_dev('core_0_uart_0','uart_file_0','log.txt')

	def test_flow(self):
		self.ac.run_script(2)
		self.dev_init()
		self.ac.run(4)
		self.ac.stop(2)
		self.ac.compare("log.txt",self.expect()[0])
		self.ac.output_result(1)
	def test_main(self):
		self.test_flow()	
	def expect(self):
		expected_output=['''

        matrix calculation
-----------------------------------------------
[15  25  48  ]     [40  39  16  ]     [764439  156  1622  ]
[0  10  -34  ]  *  [87  -21  86  ]  =  [-538642  -278  1404  ]
[-41  13  654  ]     [15868  2  -16  ]     [10377163  -564  -10002  ]
Calculation results:
[764439  156  1622  ]
[-538642  -278  1404  ]
[10377163  -564  -10002  ]


       bubble sort (little->big)
-----------------------------------------------
[102,4,9999,12,87,46,87,98]  =>  [4,12,46,87,87,98,102,9999]
        matrix calculation
-----------------------------------------------
[15  25  48  ]     [40  39  16  ]     [764439  156  1622  ]
[0  10  -34  ]  *  [87  -21  86  ]  =  [-538642  -278  1404  ]
[-41  13  654  ]     [15868  2  -16  ]     [10377163  -564  -10002  ]
Calculation results:
[764439  156  1622  ]
[-538642  -278  1404  ]
[10377163  -564  -10002  ]
'''
]
		return expected_output



