# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_C
  "/home/ksh/emscripten/git/jskyeye/utils/main/hello.c" "/home/ksh/emscripten/git/jskyeye/CMakeFiles/jskyeye.html.dir/utils/main/hello.c.o"
  )
SET(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "MODET"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/ksh/emscripten/git/jskyeye/CMakeFiles/cortex_a8.dir/DependInfo.cmake"
  "/home/ksh/emscripten/git/jskyeye/CMakeFiles/stm32.dir/DependInfo.cmake"
  "/home/ksh/emscripten/git/jskyeye/CMakeFiles/common.dir/DependInfo.cmake"
  "/home/ksh/emscripten/git/jskyeye/CMakeFiles/ram.dir/DependInfo.cmake"
  "/home/ksh/emscripten/git/jskyeye/CMakeFiles/image.dir/DependInfo.cmake"
  "/home/ksh/emscripten/git/jskyeye/CMakeFiles/uart_term.dir/DependInfo.cmake"
  "/home/ksh/emscripten/git/jskyeye/CMakeFiles/uart_leon2.dir/DependInfo.cmake"
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "arch/arm/common"
  "common/include"
  "."
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
